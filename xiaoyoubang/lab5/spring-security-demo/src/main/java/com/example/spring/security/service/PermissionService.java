package com.example.spring.security.service;

import com.example.spring.security.entity.Permission;
import org.springframework.lang.NonNull;

import java.util.List;

public interface PermissionService {
    List<Permission> getPermissions(@NonNull Long roleId);
}
