package com.example.spring.security.service.impl;

import com.example.spring.security.entity.Role;
import com.example.spring.security.repository.RoleRepository;
import com.example.spring.security.service.RoleService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleRepository roleRepository;

    @Override
    public List<Role> getRoles(@NonNull Long userId) {
        return roleRepository.getRoles(userId);
    }
}
