package com.tudou.springbootmybatis.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaoyoubang
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String orderNo;

    private Long orderItemId;

    private Long productId;

    private Long addressId;

    private BigDecimal totalPrice;

    private Integer totalCount;

    private LocalDateTime orderTime;


}
