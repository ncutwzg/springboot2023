package com.tudou.springbootmybatis.service.impl;

import com.tudou.springbootmybatis.entity.Order;
import com.tudou.springbootmybatis.mapper.OrderMapper;
import com.tudou.springbootmybatis.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaoyoubang
 * @since 2023-11-28
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

}
