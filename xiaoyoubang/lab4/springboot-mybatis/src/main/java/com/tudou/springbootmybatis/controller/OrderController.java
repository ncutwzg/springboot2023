package com.tudou.springbootmybatis.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiaoyoubang
 * @since 2023-11-28
 */
@RestController
@RequestMapping("/order")
public class OrderController {

}

