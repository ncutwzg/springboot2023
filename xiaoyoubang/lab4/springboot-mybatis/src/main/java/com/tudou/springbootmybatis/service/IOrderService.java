package com.tudou.springbootmybatis.service;

import com.tudou.springbootmybatis.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaoyoubang
 * @since 2023-11-28
 */
public interface IOrderService extends IService<Order> {

}
