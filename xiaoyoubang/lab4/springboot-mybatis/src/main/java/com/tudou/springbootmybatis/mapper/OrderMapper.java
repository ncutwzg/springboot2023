package com.tudou.springbootmybatis.mapper;

import com.tudou.springbootmybatis.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaoyoubang
 * @since 2023-11-28
 */
public interface OrderMapper extends BaseMapper<Order> {

}
