package com.tudou.springbootmybatis;

import com.tudou.springbootmybatis.entity.Student;
import com.tudou.springbootmybatis.mapper.StudentMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SimpleDemoTest {
    @Autowired
    private StudentMapper studentMapper;

    @Test
    public void testSelect() {
        List<Student> studentList = studentMapper.selectList(null);
        for (Student student : studentList) {
            System.out.println(student.getUsername());
        }
    }
}
