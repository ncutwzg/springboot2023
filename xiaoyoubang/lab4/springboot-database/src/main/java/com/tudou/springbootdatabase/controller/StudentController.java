package com.tudou.springbootdatabase.controller;


import com.tudou.springbootdatabase.entity.Student;
import com.tudou.springbootdatabase.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping("/add")
    public int addStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }

    @GetMapping("/list")
    public List listStudent(){
        return studentService.listStudent();
    }


    @DeleteMapping("/delete/{id}")
    public int deleteStudent(@PathVariable("id")Long id){
        return studentService.deleteStudent(id);
    }

    @PutMapping("/update")
    public int updateStudent(@RequestBody Student student){
        return studentService.updateStudent(student);
    }

    @GetMapping("/get/{id}")
    public Student getStudentById(@PathVariable("id") Long id){
        return studentService.getStudentById(id);
    }

    // 查询数据库中学生的总数
    @GetMapping("/count")
    public int countStudents() {
        return studentService.countStudents();
    }
}

/* RESTful规范代码
@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping
    public int addStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }

    @GetMapping
    public List<Student> listStudents() {
        return studentService.listStudent();
    }

    @DeleteMapping("/{id}")
    public int deleteStudent(@PathVariable("id") Long id) {
        return studentService.deleteStudent(id);
    }

    @PutMapping
    public int updateStudent(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable("id") Long id) {
        return studentService.getStudentById(id);
    }

    @GetMapping("/count")
    public long countStudents() {
        return studentService.countStudents();
    }
}
 */
