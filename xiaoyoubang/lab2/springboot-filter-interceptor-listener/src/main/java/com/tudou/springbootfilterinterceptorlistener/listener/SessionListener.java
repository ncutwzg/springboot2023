package com.tudou.springbootfilterinterceptorlistener.listener;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@Component
public class SessionListener implements HttpSessionListener {
    public static Integer count = 0;

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        System.out.println("新用户上线");
        count++;
        se.getSession().getServletContext().setAttribute("count", count);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        System.out.println("用户下线!");
        count--;
        se.getSession().getServletContext().setAttribute("count", count);
    }
}
