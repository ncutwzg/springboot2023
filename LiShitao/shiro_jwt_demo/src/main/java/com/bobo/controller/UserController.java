package com.bobo.controller;

import com.bobo.entity.Result;
import com.bobo.entity.Student;
import com.bobo.service.StudentService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@Api("学生管理")
public class UserController {

    @Autowired
    private StudentService service;

    @RequiresAuthentication
    @GetMapping("/main/inquire")
    public Result<List<Student>> inquire(){
        List<Student> students = service.inquire();
        return Result.success(students);
    }

    @RequiresAuthentication
    @GetMapping("/main/select")
    public Result<List<Student>> select(String type,String message){
        List<Student> students = service.select(type,message);
        return Result.success(students);
    }



    @RequiresAuthentication
    @RequiresPermissions("manage")
    @PostMapping("/manage/insert")
    public Result insert(@RequestBody Student student){
        service.insert(student);
        return Result.success();
    }

    @RequiresAuthentication
//    @RequiresPermissions("manage")
    @RequiresRoles("administrator")
    @PostMapping("/administrator/update")
    public Result update(@RequestBody Student student){
        service.update(student);
        return Result.success();
    }

    @RequiresAuthentication
    @RequiresRoles("administrator")
    @DeleteMapping("/administrator/delete")
    public Result delete(String id) {
        service.delete(id);
        return Result.success();
    }
}
