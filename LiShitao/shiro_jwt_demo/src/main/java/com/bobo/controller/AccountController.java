package com.bobo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bobo.entity.Account;
import com.bobo.entity.Md5Util;
import com.bobo.jwt.utils.JwtUtil;
import com.bobo.req.UserLoginDTO;
import com.bobo.res.ResultDTO;
import com.bobo.service.AccountService;
import io.swagger.annotations.Api;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
//@Api(tags = "AccountController", description = "账户管理,用来登录和认证")
public class AccountController {

    @Resource
    private JwtUtil jwtUtil;

    @Autowired
    private AccountService accountService;

    @PostMapping("/login")
    public ResultDTO login(@RequestBody @Validated UserLoginDTO userLoginDTO, HttpServletResponse response) {
        String username = userLoginDTO.getUsername();
        String password = userLoginDTO.getPassword();
        String MD5Util_password = Md5Util.getMD5String(password);
        Account account = accountService.getOne(new LambdaQueryWrapper<Account>().eq(Account::getUsername, username));
        if(account == null || !account.getPassword().equals(MD5Util_password)) {
            return ResultDTO.error("用户名或密码错误");
        }
        String token = jwtUtil.generateToken(username);
        response.setHeader(JwtUtil.HEADER,token);
        response.setHeader("Access-Control-Expose-Headers",JwtUtil.HEADER);
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        if (account.getPerms() != null)
        {
            if (account.getRole() != null)
            {
                map.put("Premission",account.getRole());
            } else {
                map.put("Premission",account.getPerms());
            }
        }
        return ResultDTO.success(map);
    }

    @RequiresAuthentication
    @GetMapping("/logout")
    public ResultDTO logout() {
        // 退出登录
        SecurityUtils.getSubject().logout();
        return ResultDTO.success();
    }

}
