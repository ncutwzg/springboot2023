package com.bobo.jwt.realm;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bobo.entity.Account;
import com.bobo.enums.ResponseCodeEnum;
import com.bobo.exception.BaseException;
import com.bobo.jwt.model.JwtToken;
import com.bobo.jwt.utils.JwtUtil;
import com.bobo.service.AccountService;
import io.jsonwebtoken.Claims;
import jakarta.annotation.Resource;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountRealm extends AuthorizingRealm {

    @Resource
    private JwtUtil jwtUtil;

    @Autowired
    private AccountService accountService;

    /**
     * 多重写一个support
     * 标识这个Realm是专门用来验证JwtToken
     * 不负责验证其他的token（UsernamePasswordToken）
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String jwt =(String)authenticationToken.getCredentials();
        //从jwt中获用户名
        String username = jwtUtil.getClaimsByToken(jwt).getSubject();
        //查询用户
        Account account = accountService.getOne(new LambdaQueryWrapper<Account>().eq(Account::getUsername, username));
        if(account == null){
            throw new BaseException(ResponseCodeEnum.BAD_REQUEST,"用户不存在");
        }

        Claims claims = jwtUtil.getClaimsByToken(jwt);
        if(jwtUtil.isTokenExpired(claims.getExpiration())) {
            throw new BaseException(ResponseCodeEnum.BAD_REQUEST,"token过期");
        }
        return new SimpleAuthenticationInfo(account,jwt,getName());
    }

    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        String message = principalCollection.toString();
        String result = message.substring(message.indexOf("username=")+9);
        String username = result.substring(0,result.indexOf(","));

        Account account = accountService.getOne(new LambdaQueryWrapper<Account>().eq(Account::getUsername, username));
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addRole(account.getRole());
        info.addStringPermission(account.getPerms());
        return info;
    }
}
