package com.bobo.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bobo.entity.Account;
import com.bobo.mapper.AccountMapper;
import org.springframework.stereotype.Service;

@Service
public class AccountService extends ServiceImpl<AccountMapper, Account> {
}
