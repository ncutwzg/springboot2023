package com.bobo.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bobo.entity.Student;
import com.bobo.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService extends ServiceImpl<StudentMapper, Student> {

    @Autowired
    private StudentMapper studentMapper;

    public List<Student> inquire() {
        try {
            QueryWrapper<Student> wrapper = new QueryWrapper<>();
            return studentMapper.selectList(wrapper);
        } catch (Exception e) {
            throw new RuntimeException("查询失败");
        }
    }

    public List<Student> select(String type,String message) {
        try {
            QueryWrapper<Student> wrapper = new QueryWrapper<>();
            wrapper.eq(type,message);
            List<Student> students = new ArrayList<>();
            switch (type) {
                case "id":
                case "sex":
                case "age":
                case "dept":
                    students =  studentMapper.selectList(wrapper);
                    break;
                case "name":
                    students = studentMapper.selectLike(message);
                default:
                    break;
            }
            return students;
        } catch (Exception e) {
            throw new RuntimeException("查询失败");
        }
    }

    public void update(Student student) {
        try {
            QueryWrapper<Student> wrapper = new QueryWrapper<>();
            wrapper.eq("id",student.getId());
             studentMapper.selectById(student.getId());
            studentMapper.update(student,wrapper);
        } catch (Exception e){
            throw new RuntimeException("更新失败");
        }

        try {
            QueryWrapper<Student> wrapper = new QueryWrapper<>();
            wrapper.eq("id",student.getId());
            studentMapper.update(student,wrapper);
        } catch (Exception e){
            throw new RuntimeException("更新失败");
        }
    }

    public void insert(Student student) {
        try {
            QueryWrapper<Student> wrapper = new QueryWrapper<>();
//            Student student_last = studentMapper.selectlast();
//            student.setId(student_last.getId()+1);
            studentMapper.insert(student);
        } catch (Exception e){
            throw new RuntimeException("更新失败");
        }
    }

    public void delete(String id) {
        try {
            QueryWrapper<Student> wrapper = new QueryWrapper<>();
            wrapper.eq("id",id);
            studentMapper.delete(wrapper);
        } catch (Exception e){
            throw new RuntimeException("更新失败");
        }
    }
}
