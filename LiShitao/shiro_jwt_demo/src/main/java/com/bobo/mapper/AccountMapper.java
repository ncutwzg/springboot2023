package com.bobo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bobo.entity.Account;

public interface AccountMapper extends BaseMapper<Account> {
}
