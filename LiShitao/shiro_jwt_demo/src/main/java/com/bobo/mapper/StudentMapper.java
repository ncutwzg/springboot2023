package com.bobo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bobo.entity.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface StudentMapper extends BaseMapper<Student> {


    @Select("select * from student where name like '%${message}%'")
    List<Student> selectLike( String message);

    @Insert("insert into student(name,age,sex,dept) values(#{name},#{age},#{sex},#{dept})")
    void insertNotId(Student student);

    @Select("select * from student order by id desc limit 1")
    Student selectlast();
}
