package com.bobo.entity;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@ApiModel(value = "用户实体类")
public class Account implements Serializable {
    private static final long serialVersionUID = -1L;
    private Integer id;
    private String username;
    private String password;
    private String perms;       //角色权限
    private String role;        //用户被赋予的角色
}
