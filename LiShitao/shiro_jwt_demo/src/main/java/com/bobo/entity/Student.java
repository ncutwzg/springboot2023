package com.bobo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@ApiModel(value = "Student", description = "学生实体类")
public class Student implements Serializable {
    private static final long serialVersionUID = -1L;
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;
    private String name;
    private String sex;
    private String age;
    private String dept;
}
