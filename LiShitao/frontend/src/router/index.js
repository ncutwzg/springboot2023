import { createRouter, createWebHistory } from 'vue-router'
import { ElMessage } from 'element-plus';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/Home.vue'),
      meta: {
        permission: ['null','manage','administrator'],
      }
    }, {
      path: '/login',
      name: 'login',
      component: () => import('../views/Login.vue')
    }, {
      path: '/inquire',
      name: 'inquire',
      component: () => import('../views/Inquire.vue'),
      meta: {
        permission: ['null','manage','administrator'],
      }
    }, {
      path: '/select',
      name: 'select',
      component: () => import('../views/Select.vue'),
      meta: {
        permission: ['null','manage','administrator'],
      }
    }, {
      path: '/add',
      name: 'add',
      component: () => import('../views/Add.vue'),
      meta: {
        permission: ['manage','administrator'],
      },
    }, {
      path: '/correction',
      name: 'correction',
      component: () => import('../views/Correction.vue'),
      meta: {
        permission: ['administrator'],
      }
    }
  ]
})

const CheckPermission = (to, from, next) => {
  if (to.meta?.permission) {
    const Premission = localStorage.getItem("Premission");
    const Atuh = localStorage.getItem("atuh");
    if (Premission){

      if (to.meta?.permission.includes(Premission)) {
        return next()
      } else {
        ElMessage.error("无权限");
        return next('/')
      }

    } else {
      ElMessage.error('无权限，请先登录')
      return next('/login')
    }

  } else {
    return next();
  }
}

router.beforeEach(CheckPermission)

export default router
