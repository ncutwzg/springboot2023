package com.management.app.infrastructure.consts;

public interface AppConst {

    /**
     * 默认返回的分页页号
     */
    int DEFAULT_NUMBER = 0;

    /**
     * 默认返回的分页大小
     */
    int DEFAULT_SIZE = 5;

    /**
     * 登录时需要的头
     */
    String AUTH_HEADER = "auth";
}
