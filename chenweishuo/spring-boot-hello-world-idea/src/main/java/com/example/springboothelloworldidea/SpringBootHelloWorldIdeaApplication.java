package com.example.springboothelloworldidea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringBootHelloWorldIdeaApplication {

    @RequestMapping("/")
    public String index() {
        return "Hello Spring Boot!,I am chen wei shuo!";
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringBootHelloWorldIdeaApplication.class, args);
    }

}