package com.example.spring.security;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderTest {
    @Test
    public void encodePwd() {
        String password = "$2a$10$iDwFLe0bY86qwHlkwN7KguPgLWCnU2TJ0neIIcQm4v8oAwmrN9hg6";
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encryptedPassword = passwordEncoder.encode(password);
        passwordEncoder.matches(password, encryptedPassword);
        System.out.println(encryptedPassword);
    }
}
