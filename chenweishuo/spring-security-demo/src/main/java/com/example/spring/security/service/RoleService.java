package com.example.spring.security.service;

import com.example.spring.security.entity.Role;
import org.springframework.lang.NonNull;

import java.util.List;

public interface RoleService {
    List<Role> getRoles(@NonNull Long userId);
}
