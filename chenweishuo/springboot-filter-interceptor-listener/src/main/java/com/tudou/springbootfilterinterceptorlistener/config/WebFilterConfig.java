package com.tudou.springbootfilterinterceptorlistener.config;


import com.tudou.springbootfilterinterceptorlistener.filter.RequestTest1Filter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
public class WebFilterConfig {

    @Bean
    public FilterRegistrationBean filterRegistTest1() {
        FilterRegistrationBean<Filter> frBean = new FilterRegistrationBean<>();
        RequestTest1Filter requestTest1Filter = new RequestTest1Filter();
        frBean.setFilter(requestTest1Filter);
        frBean.addUrlPatterns("/html/*");
        frBean.addInitParameter("name", "filter-test1");
        frBean.setOrder(4);
        return frBean;
    }

}
