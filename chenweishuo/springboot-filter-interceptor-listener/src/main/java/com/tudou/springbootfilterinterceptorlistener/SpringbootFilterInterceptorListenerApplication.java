package com.tudou.springbootfilterinterceptorlistener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootFilterInterceptorListenerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFilterInterceptorListenerApplication.class, args);
	}

}
