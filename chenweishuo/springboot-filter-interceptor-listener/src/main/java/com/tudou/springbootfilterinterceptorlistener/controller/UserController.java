package com.tudou.springbootfilterinterceptorlistener.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RestController
@RequestMapping("/listener")
public class UserController {

    @GetMapping("/countOnlineUser")
    public String getOnlineUserCount(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie;
        try {
            cookie = new Cookie("JSSIONID", URLEncoder.encode(request.getSession().getId(), "utf-8"));
            cookie.setPath("/");
            cookie.setMaxAge(24*60*60);
            response.addCookie(cookie);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Integer count = (Integer)request.getSession().getServletContext().getAttribute("count");
        return "total user online" + count;
    }
}
