package com.tudou.springbootmybatis.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author CWS
 * @since 2023-12-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long productId;

    private Integer count;


}
