package com.tudou.springbootmybatis.entity;

public class Student {
    private Integer id;
    private String name;

    private String url;

    private Integer alexa;
    private String country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String username) {
        this.name = name;
    }

    public String getPassword() {
        return url;
    }

    public void setPassword(String url) {
        this.url = url;
    }

    public Integer getAlexa() {
        return alexa;
    }

    public void setAlexa(Integer alexa) {
        this.alexa = alexa;
    }

    public String getPhone() {
        return country;
    }

    public void setPhone(String country) {
        this.country = country;
    }
}
