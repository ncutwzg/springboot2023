package com.tudou.springbootmybatis.service;

import com.tudou.springbootmybatis.entity.Address;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CWS
 * @since 2023-12-05
 */
public interface IAddressService extends IService<Address> {

}
