package com.tudou.springbootmybatis.mapper;

import com.tudou.springbootmybatis.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author CWS
 * @since 2023-12-05
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
