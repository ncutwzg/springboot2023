package com.tudou.springbootmybatis.mapper;

import com.tudou.springbootmybatis.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author CWS
 * @since 2023-12-05
 */
public interface OrderMapper extends BaseMapper<Order> {

}
