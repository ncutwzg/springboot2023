package org.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.pojo.Student;

public interface StudentMapper extends BaseMapper<Student> {
}
//连接对应的student表和Student实体类
