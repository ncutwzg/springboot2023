package com.example.demo.controller;

import com.example.demo.entity.Player;
import com.example.demo.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/player")
public class PlayerController {
    @Autowired
    private PlayerService playerservice;

    @RequestMapping("/add")
    public int addPlayer(@RequestBody Player player) {
        return playerservice.addPlayer(player);
    }

    @GetMapping("/list")
    public List listPlayer(){
        return playerservice.listPlayer();
    }


    @DeleteMapping("/delete/{id}")
    public int deletePlayer(@PathVariable("id")Long id){
        return playerservice.deletePlayer(id);
    }

    @PutMapping("/update")
    public int updatePlayer(@RequestBody Player student){
        return playerservice.updatePlayer(student);
    }

    @GetMapping("/get/{id}")
    public Player getPlayerById(@PathVariable("id") Long id){
        return playerservice.getPlayerById(id);
    }
}
