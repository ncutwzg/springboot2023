package com.example.demo.dao.impl;

import com.example.demo.dao.PlayerDao;
import com.example.demo.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class PlayerDaoImpl implements PlayerDao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List<Map<String, Object>> listPlayer() {
        return jdbcTemplate.queryForList("select * from player");
    }

    @Override
    public int addPlayer(Player player) {
        String sql = "insert into player (name, game_id, log_days, level) values (?,?,?,?)";
        int row = jdbcTemplate.update(sql,player.get_name(), player.get_game_id(),  player.get_log_days(), player.get_level());
        return row;
    }

    @Override
    public int deletePlayer(Long id) {
        String sql = "delete from player where id = ?";
        return jdbcTemplate.update(sql, id);
    }

    @Override
    public int updatePlayer(Player player) {
        String sql = "update player set name = ?,game_id = ?, log_days = ?,level = ? where id = ?";
        int row = jdbcTemplate.update(sql,player.get_name(),  player.get_game_id(), player.get_log_days(), player.get_level(), player.get_id());
        return row;
    }

    @Override
    public Player getPlayerById(Long id) {
        String sql = "select * from player where id = ?";
        return jdbcTemplate.queryForObject(sql, new RowMapper<Player>() {
            @Override
            public Player mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                Player player = new Player();
                player.set_id((long) resultSet.getInt(1));
                player.set_name(resultSet.getInt(2));
                player.set_game_id(resultSet.getString(3));
                player.set_log_days(resultSet.getInt(4));
                player.set_level(resultSet.getInt(5));
                return player;
            }
        }, id);
    }

}
