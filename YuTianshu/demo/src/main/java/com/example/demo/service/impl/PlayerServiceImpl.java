package com.example.demo.service.impl;

import com.example.demo.dao.PlayerDao;
import com.example.demo.entity.Player;
import com.example.demo.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    private PlayerDao playerDao;

    @Override
    public List<Map<String, Object>> listPlayer() {
        return playerDao.listPlayer();
    }

    @Override
    public int addPlayer(Player player) {
        return playerDao.addPlayer(player);  // 使用实例化对象调用静态方法
    }

    @Override
    public int deletePlayer(Long id) {
        return playerDao.deletePlayer(id);
    }

    @Override
    public int updatePlayer(Player player) {
        return playerDao.updatePlayer(player);
    }

    @Override
    public Player getPlayerById(Long id) {
        return playerDao.getPlayerById(id);
    }
}