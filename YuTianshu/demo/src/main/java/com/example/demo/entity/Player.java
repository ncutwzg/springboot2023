package com.example.demo.entity;


public class Player {
    private Long id;

    private Integer name;

    private String game_id;

    private Integer log_days;

    private Integer level;

    public Long get_id(){
        return id;
    }

    public void set_id(Long id){
        this.id = id;
    }

    public Integer get_name(){
        return name;
    }

    public void set_name(Integer name){
        this.name = name;
    }

    public String get_game_id(){
        return game_id;
    }

    public void set_game_id(String game_id){
        this.game_id = game_id;
    }

    public Integer get_log_days(){
        return log_days;
    }

    public void set_log_days(int log_days){
        this.log_days = log_days;
    }

    public Integer get_level(){
        return level;
    }

    public void set_level(Integer level){
        this.level = level;
    }
}
