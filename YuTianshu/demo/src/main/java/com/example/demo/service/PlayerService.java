package com.example.demo.service;

import com.example.demo.entity.Player;

import java.util.List;
import java.util.Map;

public interface PlayerService {
    List<Map<String, Object>> listPlayer();

    int addPlayer(Player player);

    int deletePlayer(Long id);

    int updatePlayer(Player player);

    Player getPlayerById(Long id);
}
