import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:8080';
axios.defaults.timeout = 10000;

export const listPlayer = () => {
  return axios.get('/player');
};
