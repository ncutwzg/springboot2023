import Vue from 'vue'
import Router from 'vue-router'
import player from '@/components/player'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/player', // 设置默认指向的路径
      name: 'default'
    },
    {
      path: '/player',
      name: 'player',
      component: player
    }
  ]
})
