package com.xcback.admin.service.impl;

import com.xcback.admin.dao.SysUserRoleDao;
import com.xcback.admin.entity.SysUserRole;
import com.xcback.admin.service.SysUserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service  // 告诉Spring这是一个服务类
public class SysUserRoleServiceImpl implements SysUserRoleService {

    @Resource  // 告诉Spring自动注入SysUserRoleDao的实例
    private SysUserRoleDao sysUserRoleDao;

    @Override  // 重写SysUserRoleService接口的deleteBatch方法
    public Integer deleteBatch(Long[] ids) {
        // 调用sysUserRoleDao的deleteBatch方法，将ids数组中的所有id对应的SysUserRole记录从数据库中删除
        return sysUserRoleDao.deleteBatch(ids);
    }

    @Override  // 重写SysUserRoleService接口的insertBatch方法
    public Integer insertBatch(List<SysUserRole> sysUserRoleList) {
        // 初始化计数器cnt为0
        int cnt = 0;
        // 使用for-each循环遍历sysUserRoleList列表
        for (SysUserRole sysUserRole : sysUserRoleList) {
            // 调用sysUserRoleDao的insertSelective方法，将sysUserRole插入到数据库中
            // 并将插入操作的结果添加到cnt上
            cnt += sysUserRoleDao.insertSelective(sysUserRole);
        }
        // 返回cnt，表示插入操作的总结果
        return cnt;
    }

    @Override  // 重写SysUserRoleService接口的deleteBatchByRoleId方法
    public Integer deleteBatchByRoleId(Long[] ids) {
        // 这个方法当前没有实现，所以直接返回null
        return null;
    }
}

