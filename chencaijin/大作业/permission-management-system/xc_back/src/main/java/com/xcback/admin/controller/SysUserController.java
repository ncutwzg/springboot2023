package com.xcback.admin.controller;

// 导入相关的类
import com.xcback.admin.entity.SysUser; // 系统用户实体类
import com.xcback.admin.service.SysUserRoleService; // 系统用户角色服务类
import com.xcback.admin.service.SysUserService; // 系统用户服务类
import com.xcback.common.constant.Constant; // 通用常量类
import com.xcback.common.entity.BackResult; // 通用返回结果类
import com.xcback.admin.entity.PageRequest; // 分页请求实体类
import com.xcback.utils.DateUtil; // 日期工具类
import com.xcback.utils.PageUtil; // 分页工具类
import org.apache.commons.io.FileUtils; // 文件工具类
import org.springframework.beans.factory.annotation.Value; // Spring值注解
import org.springframework.security.access.prepost.PreAuthorize; // Spring Security权限注解
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder; // Spring Security密码编码器
import org.springframework.transaction.annotation.Transactional; // Spring事务注解
import org.springframework.web.bind.annotation.*; // Spring Web注解
import org.springframework.web.multipart.MultipartFile; // Spring文件上传类

import javax.annotation.Resource; // javax注解
import java.io.File; // Java文件类
import java.io.IOException; // Java IO异常类
import java.util.Date; // Java日期类
import java.util.HashMap; // Java哈希图类
import java.util.Map; // Java映射类

/**
 * 用户 Controller 控制器
 */
@RestController // 表明这是一个REST控制器
@RequestMapping("/sys/user") // 映射请求路径
public class SysUserController {

    @Resource // 自动注入SysUserService
    private SysUserService sysUserService;

    @Resource // 自动注入SysUserRoleService
    private SysUserRoleService sysUserRoleService;

    @Resource // 自动注入BCryptPasswordEncoder
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Value("${avatarImageFilePath}") // 注入头像图片文件路径
    private String avatarImageFilePath;

    /**
     * 保存用户信息
     * @return BackResult
     */
    @PostMapping("/save") // 映射POST请求到/save
    @PreAuthorize("hasAuthority('system:user:add') || hasAuthority('system:user:edit')") // 需要'system:user:add'或'system:user:edit'权限才能访问
    public BackResult save(@RequestBody SysUser sysUser){ // 保存用户信息
        sysUser.setUpdateTime(new Date()); // 设置更新时间
        Integer cnt = sysUserService.save(sysUser); // 保存用户
        if(cnt != 1) return BackResult.failure(500,"保存失败，请联系管理员"); // 如果保存失败，返回失败结果
        return BackResult.success("保存成功!"); // 如果保存成功，返回成功结果
    }

    @PostMapping("/add") // 映射POST请求到/add
    @PreAuthorize("hasAuthority('system:user:add') || hasAuthority('system:user:edit')") // 需要'system:user:add'或'system:user:edit'权限才能访问
    public BackResult insert(@RequestBody SysUser sysUser){ // 插入用户信息
        sysUser.setUpdateTime(new Date()); // 设置更新时间
        sysUser.setCreateTime(new Date()); // 设置创建时间
        sysUser.setPassword(bCryptPasswordEncoder.encode(sysUser.getPassword())); // 设置加密后的密码
        Integer cnt = sysUserService.insert(sysUser); // 插入用户
        if(cnt != 1) return BackResult.failure(500,"添加失败"); // 如果插入失败，返回失败结果
        return BackResult.success("添加成功"); // 如果插入成功，返回成功结果
    }

    /**
     * 批量删除 |单个删除 用户信息
     * @param ids 用户id数组
     * @return 删除结果
     */
    @Transactional // 开启事务
    @PostMapping("/delete") // 映射POST请求到/delete
    @PreAuthorize("hasAuthority('system:user:delete')") // 需要'system:user:delete'权限才能访问
    public BackResult delete(@RequestBody Long[] ids){ // 删除用户信息
        Integer cnt = sysUserService.deleteBatch(ids); // 批量删除用户
        cnt += sysUserRoleService.deleteBatch(ids); // 批量删除用户角色
        if(cnt>0) return BackResult.success("删除成功！"); // 如果删除成功，返回成功结果
        return BackResult.failure(500,"删除失败请联系管理人员！"); // 如果删除失败，返回失败结果
    }

    /**
     * 根据主键id查询用户信息
     * @param id 用户id
     * @return 用户信息
     */
    @GetMapping("/{id}") // 映射GET请求到/{id}
    @PreAuthorize("hasAuthority('system:user:query')") // 需要'system:user:query'权限才能访问
    public BackResult findById(@PathVariable("id") Integer id){ // 根据id查询用户信息
        SysUser sysUser = sysUserService.selectByPrimaryKey(id.longValue()); // 查询用户
        return BackResult.success(sysUser); // 返回成功结果
    }

    /**
     * 验证用户名是否已存在
     * @param sysUser 系统用户对象
     * @return 验证结果
     */
    @PostMapping("/checkUserName") // 映射POST请求到/checkUserName
    @PreAuthorize("hasAuthority('system:user:query')") // 需要'system:user:query'权限才能访问
    public BackResult checkUserName(@RequestBody SysUser sysUser){ // 验证用户名是否已存在
        if(sysUserService.getByUserName(sysUser.getUsername())!=null) // 如果用户名已存在
            return BackResult.failure(400,"用户名已存在！"); // 返回失败结果
        return BackResult.success(); // 返回成功结果
    }


    /**
     * 重置密码
     * @param id 用户ID
     * @return 返回操作结果
     */
    @GetMapping("/resetPassword/{id}")
    @PreAuthorize("hasAuthority('system:user:edit')")
    public BackResult resetPassword(@PathVariable(value = "id")Integer id){
        // 根据用户ID查询用户信息
        SysUser sysUser = sysUserService.selectByPrimaryKey(id.longValue());
        // 将用户密码重置为默认密码，并进行加密
        sysUser.setPassword(bCryptPasswordEncoder.encode(Constant.DEFAULT_PASSWORD));
        // 更新用户信息的修改时间
        sysUser.setUpdateTime(new Date());
        // 保存修改后的用户信息
        Integer cnt = sysUserService.save(sysUser);
        // 如果保存失败，返回错误信息
        if(cnt==0) return BackResult.failure(500,"密码重置有误,请联系管理员!");
        // 如果保存成功，返回成功信息
        return BackResult.success("密码重置成功!");
    }

    /**
     * 修改密码
     * @param rawSysUser 包含用户ID和新旧密码的用户对象
     * @return 返回操作结果
     */
    @PostMapping("/updateUserPwd")
    @PreAuthorize("hasAuthority('system:user:edit')")
    public BackResult updatePwd(@RequestBody SysUser rawSysUser){
        // 根据用户ID查询用户信息
        SysUser encodedSysUser = sysUserService.selectByPrimaryKey(rawSysUser.getId());
        // 验证旧密码是否正确
        if(bCryptPasswordEncoder.matches(rawSysUser.getOldPassword(),encodedSysUser.getPassword())){
            // 更新用户信息的修改时间
            rawSysUser.setUpdateTime(new Date());
            // 将新密码进行加密后设置到用户对象中
            rawSysUser.setPassword(bCryptPasswordEncoder.encode(rawSysUser.getNewPassword()));
            // 保存修改后的用户信息
            Integer cnt = sysUserService.save(rawSysUser);
            // 如果保存成功，返回成功信息，否则返回错误信息
            return (cnt==1)?(BackResult.success("修改密码成功")):(BackResult.failure(500,"修改失败，请联系管理员!"));
        }
        // 如果旧密码验证失败，返回错误信息
        return BackResult.failure(400,"输入旧密码错误！");
    }

    @GetMapping("/updateStatus/{id}/status/{status}")
    @PreAuthorize("hasAuthority('system:user:edit')")
    public BackResult updateStatus(@PathVariable("id") Integer id,
                                   @PathVariable("status") Character status){
        SysUser sysUser = sysUserService.selectByPrimaryKey(id.longValue());
        sysUser.setUpdateTime(new Date());
        sysUser.setStatus(status.toString());
        Integer cnt = sysUserService.save(sysUser);
        if(cnt==0) return BackResult.failure(500,"状态改变有误,请联系管理员!");
        return BackResult.success("状态修改成功!");
    }
    @PostMapping("/uploadImage")
    @PreAuthorize("hasAuthority('system:user:edit')")
    public Map<String,Object> uploadImage(@RequestParam("file") MultipartFile file) {
        Map<String,Object> result = new HashMap<>();
        try {
            if (!file.isEmpty()) {
                // 获取文件名
                String originalFilename = file.getOriginalFilename();
                String suffixName = originalFilename.substring(originalFilename.lastIndexOf("."));
                String newFileName = DateUtil.currentTimeStr()+suffixName;
                        FileUtils.copyInputStreamToFile(file.getInputStream(), new File(avatarImageFilePath + newFileName));
                result.put("status",200);
                result.put("msg","上传成功！");
                Map<String,Object> data = new HashMap<>();
                data.put("title",newFileName);
                data.put("src","image/userAvatar/"+newFileName);
                result.put("data",data);
            }
        }catch(IOException e){
            result.put("status",500);
            result.put("msg","上传失败，请联系工作人员！");
        }finally {
            return result;
        }
    }
    /**
     * 更新头像
     * @param sysUser
     * @return
     */
    @PostMapping("/updateAvatar")
    @PreAuthorize("hasAuthority('system:user:edit')")
    public BackResult updateAvatar(@RequestBody SysUser sysUser){
        SysUser currentUser = sysUserService.selectByPrimaryKey(sysUser.getId());
        currentUser.setAvatar(sysUser.getAvatar());
        Integer cnt = sysUserService.save(currentUser);
        return (cnt==1)?(BackResult.success("头像更换成功！")):(BackResult.failure(500,"头像更换失败，请联系管理员！"));
    }
    /**
     * 根据条件分页查询用户信息
     * @param pageRequest
     * @return
     */
    @PostMapping("/list")
    @PreAuthorize("hasAuthority('system:user:query')")
    public BackResult list(@RequestBody PageRequest pageRequest){
        try{
            PageUtil.initPageRequest(pageRequest);
            sysUserService.selectAll(pageRequest.getQuery());
            return BackResult.success(PageUtil.PAGE_RESULT);
        }catch(Throwable e){
            e.printStackTrace();
            return BackResult.failure(400,"查询有误,请检查分页信息是否有误!");
        }
    }

}
