package com.xcback.security.config;

// 导入相关的类
import com.xcback.security.exceptions.JWTAuthenticationEntryPoint; // JWT认证异常入口点
import com.xcback.security.filter.CaptchaFilter; // 验证码过滤器
import com.xcback.security.filter.JWTAuthenticationFilter; // JWT认证过滤器
import com.xcback.security.handler.LoginFailureHandler; // 登录失败处理器
import com.xcback.security.filter.LoginFilter; // 登录过滤器
import com.xcback.security.handler.LoginSuccessHandler; // 登录成功处理器
import com.xcback.security.service.MyUserDetailService; // 自定义用户详细信息服务
import org.springframework.context.annotation.Bean; // Spring Bean注解
import org.springframework.security.authentication.AuthenticationManager; // Spring Security认证管理器
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder; // Spring Security认证管理器构建器
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity; // Spring Security全局方法安全配置
import org.springframework.security.config.annotation.web.builders.HttpSecurity; // Spring Security Http安全配置
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity; // Spring Security Web安全配置
import org.springframework.security.config.http.SessionCreationPolicy; // Spring Security会话创建策略
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder; // Spring Security BCrypt密码编码器
import org.springframework.security.crypto.password.PasswordEncoder; // Spring Security密码编码器
import org.springframework.security.web.SecurityFilterChain; // Spring Security安全过滤链
import org.springframework.security.web.authentication.logout.LogoutFilter; // Spring Security注销过滤器
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler; // Spring Security注销成功处理器
import org.springframework.web.cors.CorsConfiguration; // Spring Web跨域配置
import org.springframework.web.cors.CorsConfigurationSource; // Spring Web跨域配置源
import org.springframework.web.cors.UrlBasedCorsConfigurationSource; // Spring Web基于URL的跨域配置源

import javax.annotation.Resource; // javax注解

@EnableWebSecurity // 启用Web安全
@EnableGlobalMethodSecurity(prePostEnabled = true) // 启用全局方法安全，允许在方法级别进行安全配置
public class SecurityConfig {

    @Resource // 自动注入MyUserDetailService
    private MyUserDetailService myUserDetailService;

    @Resource // 自动注入LoginSuccessHandler
    private LoginSuccessHandler loginSuccessHandler;

    @Resource // 自动注入LoginFailureHandler
    private LoginFailureHandler loginFailureHandler;

    @Resource // 自动注入LogoutSuccessHandler
    private LogoutSuccessHandler logoutSuccessHandlerImpl;

    @Resource // 自动注入JWTAuthenticationEntryPoint
    private JWTAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Resource // 自动注入CaptchaFilter
    private CaptchaFilter captchaFilter;

    private static final String[] URL_PERMITTED_LIST = { // 允许的URL列表
            "/api/auth/login",
            "/api/auth/logout",
            "/captcha",
            "/password",
            "/image/**",
            "/test/**"
    };

    @Bean // 定义一个Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        // 不开启跨站请求伪造的防护
        http.csrf(csrfConfig->csrfConfig.disable());

        // 关闭session
        // 关闭原因：
        // 1. 前后端进行通信，每个请求都是一个独立的事务，开启session管理可能会使得信息无法共享
        // 2. 采用session管理的话，多个用户进行访问服务器端的内存会占用过高，这是因为session的废除机制是超时机制
        // 3. 采用session管理功能，这也是一个安全漏洞
        // 这里使用jwt（Java web token）令牌的方式进行认证，不需要session了
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // 配置跨域，允许跨域访问
        http.cors().configurationSource(this.corsConfigurationSource());

        // 配置拦截规则
        http.authorizeRequests()
                .antMatchers(URL_PERMITTED_LIST).permitAll() // 允许访问的URL列表
                .anyRequest() // 任何请求
                .authenticated(); // 都需要认证

        // 异常处理配置
        http.exceptionHandling()
                .authenticationEntryPoint(jwtAuthenticationEntryPoint); // 设置认证异常入口点

        // 退出系统的一些配置，logout 退出系统的url，和成功处理器
        http.logout()
                .logoutUrl("/api/auth/logout") // 设置注销URL
                .logoutSuccessHandler(logoutSuccessHandlerImpl); // 设置注销成功处理器

        // 添加自定义的认证过滤器 LoginFilter
        // 前后端登录信息以JSON数据传送，替代Security中的form表单形式
        http.addFilter(loginFilter(http));

        // 添加自定义的过滤器-基本认证过滤器，让每个请求都得经过jwt认证...
        http.addFilter(jwtAuthenticationFilter(http));

        // 添加自定义过滤器-基本过滤器，验证验证码
        http.addFilterAfter(captchaFilter, LogoutFilter.class);
        return http.build(); // 构建HttpSecurity
    }



    @Bean // 定义一个Bean
    public JWTAuthenticationFilter jwtAuthenticationFilter(HttpSecurity http) throws Exception {
        return new JWTAuthenticationFilter(authenticationManager(http)); // 创建JWT认证过滤器
    }

    @Bean // 定义一个Bean
    public LoginFilter loginFilter(HttpSecurity http) throws Exception {
        LoginFilter loginFilter = new LoginFilter(authenticationManager(http)); // 创建登录过滤器
        loginFilter.setFilterProcessesUrl("/api/auth/login"); // 设置认证的url，默认是 /login
        loginFilter.setUsernameParameter("username");  // 设置参数用户名名称
        loginFilter.setPasswordParameter("password");  // 设置参数密码名称
        loginFilter.setAuthenticationSuccessHandler(loginSuccessHandler); // 设置认证成功后的处理器
        loginFilter.setAuthenticationFailureHandler(loginFailureHandler); // 设置认证失败后的处理器
        return loginFilter; // 返回登录过滤器
    }

    @Bean // 定义一个Bean
    public AuthenticationManager authenticationManager(HttpSecurity http) throws Exception {
        return http.getSharedObject(AuthenticationManagerBuilder.class) // 获取认证管理器构建器
                .userDetailsService(myUserDetailService) // 设置用户详细信息服务
                .passwordEncoder(passwordEncoder()) // 设置密码编码器
                .and().build(); // 构建认证管理器
    }

    @Bean // 定义一个Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(); // 创建BCrypt密码编码器
    }

    private CorsConfigurationSource corsConfigurationSource(){
        CorsConfiguration corsConfiguration = new CorsConfiguration(); // 创建跨域配置
        corsConfiguration.setAllowCredentials(true); // 允许凭证
        corsConfiguration.addAllowedHeader("*"); // 允许所有的头部信息
        corsConfiguration.addAllowedMethod("*"); // 允许所有的请求方法
        corsConfiguration.addExposedHeader("*"); // 暴露所有的头部信息
        corsConfiguration.addAllowedOriginPattern("http://localhost:5173/"); // 允许所有的源
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(); // 创建基于URL的跨域配置源
        source.registerCorsConfiguration("/**",corsConfiguration); // 注册跨域配置
        return source; // 返回跨域配置源
    }

}
