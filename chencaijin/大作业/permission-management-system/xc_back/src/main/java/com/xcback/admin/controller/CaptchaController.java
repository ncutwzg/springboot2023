package com.xcback.admin.controller;

// 导入相关的类
import cn.hutool.core.codec.Base64Encoder; // 用于Base64编码
import com.google.code.kaptcha.Producer; // 用于生成验证码
import com.xcback.common.constant.Constant; // 用于存储常量
import com.xcback.common.entity.BackResult; // 用于返回结果
import com.xcback.utils.RedisUtil; // 用于操作Redis
import lombok.extern.slf4j.Slf4j; // 用于日志记录
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO; // 用于处理图像
import java.awt.image.BufferedImage; // 用于创建图像
import java.io.ByteArrayOutputStream; // 用于处理字节流
import java.io.IOException; // 用于处理IO异常
import java.util.HashMap; // 用于创建HashMap
import java.util.Map; // 用于创建Map
import java.util.UUID; // 用于生成唯一的UUID

/**
 * 验证码Controller控制器
 */
@RestController // 表明这是一个REST控制器
@Slf4j // 启用日志记录
public class CaptchaController {

    @Resource // 自动注入Producer
    private Producer producer;

    @Resource // 自动注入RedisUtil
    private RedisUtil redisUtil;

    @GetMapping("/captcha") // 映射GET请求到/captcha
    public BackResult captcha() throws IOException {
        String key= UUID.randomUUID().toString(); // 生成随机唯一key
        String code = producer.createText(); // 生成验证码文本

        log.info("code--->{}",code); // 记录验证码文本

        BufferedImage image = producer.createImage(code); // 根据验证码文本生成验证码图像
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(); // 创建字节流输出流
        ImageIO.write(image, "jpg", outputStream); // 将验证码图像写入输出流

        Base64Encoder encoder = new Base64Encoder(); // 创建Base64编码器
        String str = "data:image/jpeg;base64,"; // 创建Base64图像数据前缀

        String base64Img = str + encoder.encode(outputStream.toByteArray()); // 将验证码图像转换为Base64编码

        redisUtil.hset(Constant.CAPTCHA_KEY,key,code,60*5); // 将验证码文本存储到Redis，有效期为5分钟

        Map<String,Object> resultMap=new HashMap<>(); // 创建结果Map
        resultMap.put("captchaImg",base64Img); // 将Base64编码的验证码图像添加到结果Map
        resultMap.put("uuid",key); // 将UUID添加到结果Map
        return BackResult.success(resultMap); // 返回成功的结果

    }

}
