package com.xcback.admin.service.impl;

import com.xcback.admin.annotation.PageHandler;
import com.xcback.admin.dao.SysRoleDao;
import com.xcback.admin.entity.SysRole;
import com.xcback.admin.service.SysRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service  // 告诉Spring这是一个服务类
public class SysRoleServiceImpl implements SysRoleService {

    @Resource  // 告诉Spring自动注入SysRoleDao的实例
    private SysRoleDao sysRoleDao;

    @Override  // 重写SysRoleService接口的selectAll方法
    public List<SysRole> selectAll() {
        // 调用sysRoleDao的selectAll方法，从数据库中选择所有的SysRole记录
        return sysRoleDao.selectAll();
    }

    @Override  // 重写SysRoleService接口的selectAllByRoleName方法
    @PageHandler  // 使用PageHandler注解，表示这个方法需要进行分页处理
    public List<SysRole> selectAllByRoleName(String roleName) {
        // 调用sysRoleDao的selectAllByRoleName方法，根据roleName从数据库中选择所有的SysRole记录
        return sysRoleDao.selectAllByRoleName(roleName);
    }

    @Override  // 重写SysRoleService接口的selectByRoleId方法
    public SysRole selectByRoleId(Long id) {
        // 调用sysRoleDao的selectByPrimaryKey方法，根据id从数据库中选择一个SysRole记录
        return sysRoleDao.selectByPrimaryKey(id);
    }

    @Override  // 重写SysRoleService接口的save方法
    public Integer save(SysRole sysRole) {
        // 调用sysRoleDao的updateByPrimaryKeySelective方法，将sysRole对象的信息更新到数据库中
        return sysRoleDao.updateByPrimaryKeySelective(sysRole);
    }

    @Override  // 重写SysRoleService接口的insert方法
    public Integer insert(SysRole sysRole) {
        // 调用sysRoleDao的insertSelective方法，将sysRole对象插入到数据库中
        return sysRoleDao.insertSelective(sysRole);
    }

    @Override  // 重写SysRoleService接口的delete方法
    public Integer delete(Long[] ids) {
        // 调用sysRoleDao的deleteBatch方法，将ids数组中的所有id对应的SysRole记录从数据库中删除
        return sysRoleDao.deleteBatch(ids);
    }
}
