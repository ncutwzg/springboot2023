package com.xcback.admin.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.xcback.admin.annotation.PageHandler;
import com.xcback.admin.dao.SysRoleMenuDao;
import com.xcback.admin.dao.SysUserDao;
import com.xcback.admin.dao.SysUserRoleDao;
import com.xcback.admin.entity.SysMenu;
import com.xcback.admin.entity.SysRole;
import com.xcback.admin.entity.SysUser;
import com.xcback.admin.service.SysUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Service  // 告诉Spring这是一个服务类
public class SysUserServiceImpl implements SysUserService {

    @Resource  // 告诉Spring自动注入SysUserDao的实例
    private SysUserDao sysUserDao;

    @Resource  // 告诉Spring自动注入SysUserRoleDao的实例
    private SysUserRoleDao sysUserRoleDao;

    @Resource  // 告诉Spring自动注入SysRoleMenuDao的实例
    private SysRoleMenuDao sysRoleMenuDao;

    @Override  // 重写SysUserService接口的insert方法
    public Integer insert(SysUser record) {
        // 调用sysUserDao的insert方法，将record插入到数据库中
        return sysUserDao.insert(record);
    }

    @Override  // 重写SysUserService接口的selectByPrimaryKey方法
    public SysUser selectByPrimaryKey(Long id) {
        // 调用sysUserDao的selectByPrimaryKey方法，根据id从数据库中选择一个SysUser记录
        return sysUserDao.selectByPrimaryKey(id);
    }

    @Override  // 重写SysUserService接口的selectAll方法
    @PageHandler  // 使用PageHandler注解，表示这个方法需要进行分页处理
    public List<SysUser> selectAll(String username) {
        // 调用sysUserDao的selectAll方法，根据username从数据库中选择所有的SysUser记录
        List<SysUser> sysUsers = sysUserDao.selectAll(username);
        // 使用forEach方法遍历sysUsers列表
        sysUsers.forEach(sysUser->{
            // 调用sysUserRoleDao的getUserAuthorities方法，根据sysUser的id获取其角色列表
            // 然后调用sysUser的setSysRoleList方法，将角色列表设置到sysUser对象中
            sysUser.setSysRoleList(sysUserRoleDao.getUserAuthorities(sysUser.getId()));
        });
        // 返回sysUsers列表
        return sysUsers;
    }

    @Override  // 重写SysUserService接口的getByUserName方法
    public SysUser getByUserName(String username) {
        // 调用sysUserDao的getByUserName方法，根据username从数据库中获取一个SysUser记录
        return sysUserDao.getByUserName(username);
    }

    @Override  // 重写SysUserService接口的getUserAuthoritiesInfo方法
    public String getUserAuthoritiesInfo(Long userId) {
        // 创建一个StringJoiner对象，用于连接字符串
        final StringJoiner stringJoiner = new StringJoiner(",");
        // 调用sysUserRoleDao的getUserAuthorities方法，根据userId获取用户的所有角色
        List<SysRole> roles = sysUserRoleDao.getUserAuthorities(userId);
        // 如果角色列表的大小大于0，说明用户有角色
        if(roles.size() > 0){
            // 使用stream的map方法将角色列表转换为角色代码的列表
            // 然后使用collect方法将角色代码的列表连接成一个字符串
            String roleStr = roles.stream()
                    .map(auth -> "ROLE_" + auth.getCode())
                    .collect(Collectors.joining(","));
            // 将角色字符串添加到stringJoiner中
            stringJoiner.add(roleStr);
        }
        // 创建一个HashSet对象，用于存储菜单权限代码
        final Set<String> menuCodeStrSet = new HashSet<>();
        // 使用forEach方法遍历角色列表
        roles.forEach(role->{
            // 调用sysRoleMenuDao的getMenuByRoleId方法，根据角色的id获取其所有菜单
            List<SysMenu> menus = sysRoleMenuDao.getMenuByRoleId(role.getId());
            // 使用stream的map方法将菜单列表转换为菜单权限代码的列表
            // 使用filter方法过滤掉空的菜单权限代码
            // 然后使用forEach方法将菜单权限代码添加到menuCodeStrSet中
            menus.stream()
                    .map(menu->menu.getPerms())
                    .filter(perm->!StringUtils.isEmpty(perm))
                    .forEach(menuCodeStrSet::add);
        });
        // 如果菜单权限代码的集合的大小大于0，说明有菜单权限
        if (menuCodeStrSet.size()>0) {
            // 使用forEach方法将菜单权限代码添加到stringJoiner中
            menuCodeStrSet.forEach(stringJoiner::add);
        }
        // 返回stringJoiner中的字符串，这个字符串包含了用户的所有角色和菜单权限
        return stringJoiner.toString();
    }

    @Override  // 重写SysUserService接口的save方法
    public Integer save(SysUser sysUser) {
        // 调用sysUserDao的updateByPrimaryKeySelective方法，将sysUser对象的信息更新到数据库中
        return sysUserDao.updateByPrimaryKeySelective(sysUser);
    }

    @Override  // 重写SysUserService接口的deleteBatch方法
    public Integer deleteBatch(Long[] ids) {
        // 调用sysUserDao的deleteBatch方法，将ids数组中的所有id对应的SysUser记录从数据库中删除
        return sysUserDao.deleteBatch(ids);
    }
}
