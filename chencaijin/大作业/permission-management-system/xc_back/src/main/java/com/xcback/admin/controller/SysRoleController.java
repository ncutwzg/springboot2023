package com.xcback.admin.controller;

// 导入相关的类
import com.xcback.admin.entity.PageRequest; // 分页请求实体类
import com.xcback.admin.entity.SysRole; // 系统角色实体类
import com.xcback.admin.entity.SysUserRole; // 系统用户角色实体类
import com.xcback.admin.service.SysRoleService; // 系统角色服务类
import com.xcback.admin.service.SysUserRoleService; // 系统用户角色服务类
import com.xcback.common.entity.BackResult; // 通用返回结果类
import com.xcback.utils.PageUtil; // 分页工具类
import org.springframework.security.access.prepost.PreAuthorize; // Spring Security权限注解
import org.springframework.transaction.annotation.Transactional; // Spring事务注解
import org.springframework.web.bind.annotation.*; // Spring Web注解

import javax.annotation.Resource; // javax注解
import java.util.ArrayList; // Java列表类
import java.util.Arrays; // Java数组工具类
import java.util.Date; // Java日期类
import java.util.List; // Java列表类

@RestController // 表明这是一个REST控制器
@RequestMapping("/sys/role") // 映射请求路径
public class SysRoleController {

    @Resource // 自动注入SysRoleService
    private SysRoleService sysRoleService;

    @Resource // 自动注入SysUserRoleService
    private SysUserRoleService sysUserRoleService;

    @GetMapping("/listAll") // 映射GET请求到/listAll
    @PreAuthorize("hasAuthority('system:role:query')") // 需要'system:role:query'权限才能访问
    public BackResult listAll(){ // 获取所有角色列表
        List<SysRole> roleList = sysRoleService.selectAll(); // 查询所有角色
        return BackResult.success(roleList); // 返回成功结果
    }

    @Transactional // 开启事务
    @PostMapping("/grantRole/{id}") // 映射POST请求到/grantRole/{id}
    @PreAuthorize("hasAuthority('system:user:role')") // 需要'system:user:role'权限才能访问
    public BackResult grantRole(@PathVariable("id") Long userId, // 用户id
                                @RequestBody Long[] roleIds){ // 角色id数组
        sysUserRoleService.deleteBatch(new Long[]{userId}); // 根据用户id删除用户角色
        List<SysUserRole> userRoleList = new ArrayList<>(); // 创建用户角色列表
        Arrays.stream(roleIds).forEach(roleId-> // 遍历角色id数组
                userRoleList.add(SysUserRole.builder() // 添加用户角色
                        .userId(userId) // 设置用户id
                        .roleId(roleId) // 设置角色id
                        .build()) // 构建用户角色对象
        );
        sysUserRoleService.insertBatch(userRoleList); // 批量插入用户角色
        return BackResult.success("角色分配成功!"); // 返回成功结果
    }

    /**
     * 根据条件分页查询角色信息
     * @param pageRequest 分页请求对象
     * @return 分页结果
     */
    @PostMapping("/list") // 映射POST请求到/list
    @PreAuthorize("hasAuthority('system:user:query')") // 需要'system:user:query'权限才能访问
    public BackResult list(@RequestBody PageRequest pageRequest){ // 根据条件分页查询角色信息
        PageUtil.initPageRequest(pageRequest); // 初始化分页请求
        sysRoleService.selectAllByRoleName(pageRequest.getQuery()); // 根据角色名查询所有角色
        return BackResult.success(PageUtil.PAGE_RESULT); // 返回成功结果
    }

    /**
     * 更新角色信息
     * @param sysRole 系统角色对象
     * @return 更新结果
     */
    @PostMapping("/save") // 映射POST请求到/save
    @PreAuthorize("hasAuthority('system:role:add') || hasAuthority('system:role:edit')") // 需要'system:role:add'或'system:role:edit'权限才能访问
    public BackResult save(@RequestBody SysRole sysRole){ // 更新角色信息
        sysRole.setUpdateTime(new Date()); // 设置更新时间
        Integer cnt = sysRoleService.save(sysRole); // 保存角色
        if(cnt==1) return BackResult.success("保存角色信息成功!"); // 如果保存成功，返回成功结果
        return BackResult.failure(500,"保存失败,请联系管理员"); // 如果保存失败，返回失败结果
    }

    /**
     * 添加角色信息
     * @param sysRole 系统角色对象
     * @return 添加结果
     */
    @PostMapping("/add") // 映射POST请求到/add
    @PreAuthorize("hasAuthority('system:role:add')") // 需要'system:role:add'权限才能访问
    public BackResult insert(@RequestBody SysRole sysRole){ // 添加角色信息
        sysRole.setCreateTime(new Date()); // 设置创建时间
        sysRole.setUpdateTime(new Date()); // 设置更新时间
        Integer cnt = sysRoleService.insert(sysRole); // 插入角色
        if(cnt==1) return BackResult.success("添加信息成功!"); // 如果插入成功，返回成功结果
        return BackResult.failure(500,"添加角色信息失败,请联系管理员"); // 如果插入失败，返回失败结果
    }

    /**
     * 根据角色 Id 查询角色信息
     * @param roleId 角色id
     * @return 角色信息
     */
    @GetMapping("/{id}") // 映射GET请求到/{id}
    @PreAuthorize("hasAuthority('system:role:query')") // 需要'system:role:query'权限才能访问
    public BackResult getSysRoleById(@PathVariable("id") Long roleId){ // 根据角色id查询角色信息
        return BackResult.success(sysRoleService.selectByRoleId(roleId)); // 返回成功结果
    }

    @Transactional // 开启事务
    @PreAuthorize("hasAuthority('system:role:delete')") // 需要'system:role:delete'权限才能访问
    @PostMapping("/delete") // 映射POST请求到/delete
    public BackResult delete(@RequestBody Long[] ids){ // 删除角色
        sysRoleService.delete(ids); // 删除角色
        sysUserRoleService.deleteBatchByRoleId(ids); // 根据角色id批量删除用户角色
        return BackResult.success("删除成功!"); // 返回成功结果
    }

}
