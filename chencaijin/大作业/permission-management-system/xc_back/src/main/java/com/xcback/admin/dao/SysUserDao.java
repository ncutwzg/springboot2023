package com.xcback.admin.dao;

// 导入相关的类
import com.xcback.admin.entity.SysUser; // 系统用户实体类
import org.apache.ibatis.annotations.Param; // MyBatis参数注解

import java.util.List; // Java列表类

public interface SysUserDao { // 系统用户数据访问对象接口

    int deleteBatch(@Param("ids") Long[] ids); // 批量删除用户，参数是用户id数组

    int insert(SysUser record); // 插入用户，参数是用户记录

    int insertSelective(SysUser record); // 选择性插入用户，参数是用户记录

    List<SysUser> selectAll(String username); // 查询所有用户，参数是用户名

    SysUser getByUserName(String username); // 根据用户名获取用户，参数是用户名

    SysUser selectByPrimaryKey(Long id); // 根据主键id查询用户，参数是用户id

    int updateByPrimaryKeySelective(SysUser record); // 根据主键id选择性更新用户，参数是用户记录

    int updateByPrimaryKey(SysUser record); // 根据主键id更新用户，参数是用户记录
}
