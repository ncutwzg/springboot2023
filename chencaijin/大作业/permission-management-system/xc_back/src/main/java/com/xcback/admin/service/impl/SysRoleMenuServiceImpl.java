package com.xcback.admin.service.impl;

import com.xcback.admin.dao.SysRoleMenuDao;
import com.xcback.admin.entity.SysRoleMenu;
import com.xcback.admin.service.SysRoleMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service  // 告诉Spring这是一个服务类
public class SysRoleMenuServiceImpl implements SysRoleMenuService {

    @Resource  // 告诉Spring自动注入SysRoleMenuDao的实例
    private SysRoleMenuDao sysRoleMenuDao;

    @Override  // 重写SysRoleMenuService接口的selectByRoleId方法
    public List<SysRoleMenu> selectByRoleId(Integer roleId) {
        // 调用sysRoleMenuDao的selectByRoleId方法，根据roleId从数据库中选择所有的SysRoleMenu记录
        return sysRoleMenuDao.selectByRoleId(roleId);
    }

    @Override  // 重写SysRoleMenuService接口的deleteByRoleId方法
    public Integer deleteByRoleId(Long roleId) {
        // 调用sysRoleMenuDao的deleteByRoleId方法，将roleId对应的SysRoleMenu记录从数据库中删除
        return sysRoleMenuDao.deleteByRoleId(roleId.intValue());
    }

    @Override  // 重写SysRoleMenuService接口的insertBatch方法
    public Integer insertBatch(List<SysRoleMenu> roleMenuList) {
        // 初始化计数器cnt为0
        int cnt = 0;
        // 使用for-each循环遍历roleMenuList列表
        for (SysRoleMenu sysRoleMenu : roleMenuList) {
            // 调用sysRoleMenuDao的insertSelective方法，将sysRoleMenu插入到数据库中
            // 并将插入操作的结果添加到cnt上
            cnt += sysRoleMenuDao.insertSelective(sysRoleMenu);
        }
        // 返回cnt，表示插入操作的总结果
        return cnt;
    }
}

