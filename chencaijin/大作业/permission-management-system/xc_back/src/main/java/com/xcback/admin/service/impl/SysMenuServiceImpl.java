package com.xcback.admin.service.impl;

import com.xcback.admin.dao.SysMenuDao;
import com.xcback.admin.entity.SysMenu;
import com.xcback.admin.service.SysMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@Service  // 告诉Spring这是一个服务类
public class SysMenuServiceImpl implements SysMenuService {

    @Resource  // 告诉Spring自动注入SysMenuDao的实例
    private SysMenuDao sysMenuDao;

    @Override  // 重写SysMenuService接口的buildTreeMenu方法
    public List<SysMenu> buildTreeMenu(List<? extends SysMenu> sysMenuList) {
        // 创建一个新的SysMenu列表，用于存储结果
        List<SysMenu> result = new ArrayList<>();
        // 使用for-each循环遍历sysMenuList列表
        for (SysMenu sysMenu : sysMenuList) {
            // 再次使用for-each循环遍历sysMenuList列表，寻找sysMenu的子菜单
            for (SysMenu menu : sysMenuList) {
                // 如果menu的parentId等于sysMenu的id，说明menu是sysMenu的子菜单
                if (menu.getParentId() == sysMenu.getId()) {
                    // 将menu添加到sysMenu的子菜单列表中
                    sysMenu.getChildren().add(menu);
                }
            }
            // 如果sysMenu的parentId等于0，说明sysMenu是树的根节点
            if (sysMenu.getParentId() == 0) {
                // 将sysMenu添加到结果列表中
                result.add(sysMenu);
            }
        }
        // 返回结果列表
        return result;
    }

    @Override  // 重写SysMenuService接口的selectAllAsc方法
    public List<SysMenu> selectAllAsc() {
        // 调用sysMenuDao的selectAllAsc方法，从数据库中选择所有的SysMenu记录，并按升序排序
        return sysMenuDao.selectAllAsc();
    }

    @Override  // 重写SysMenuService接口的selectById方法
    public SysMenu selectById(Integer id) {
        // 调用sysMenuDao的selectByPrimaryKey方法，根据id从数据库中选择一个SysMenu记录
        return sysMenuDao.selectByPrimaryKey(id.longValue());
    }

    @Override  // 重写SysMenuService接口的save方法
    public Integer save(SysMenu sysMenu) {
        // 调用sysMenuDao的updateByPrimaryKeySelective方法，将sysMenu对象的信息更新到数据库中
        return sysMenuDao.updateByPrimaryKeySelective(sysMenu);
    }

    @Override  // 重写SysMenuService接口的insert方法
    public Integer insert(SysMenu sysMenu) {
        // 调用sysMenuDao的insertSelective方法，将sysMenu对象插入到数据库中
        return sysMenuDao.insertSelective(sysMenu);
    }

    @Override  // 重写SysMenuService接口的deleteById方法
    public Integer deleteById(Long id) {
        // 调用sysMenuDao的deleteByPrimaryKey方法，将id对应的SysMenu记录从数据库中删除
        return sysMenuDao.deleteByPrimaryKey(id);
    }

    @Override  // 重写SysMenuService接口的countByParentId方法
    public Integer countByParentId(Long id) {
        // 调用sysMenuDao的countByParentId方法，根据id从数据库中计算SysMenu记录的数量
        return sysMenuDao.countByParentId(id);
    }
}
