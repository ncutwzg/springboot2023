package com.xcback.admin.controller;

// 导入相关的类
import com.xcback.admin.entity.SysMenu; // 系统菜单实体类
import com.xcback.admin.entity.SysRoleMenu; // 系统角色菜单实体类
import com.xcback.admin.service.SysMenuService; // 系统菜单服务类
import com.xcback.admin.service.SysRoleMenuService; // 系统角色菜单服务类
import com.xcback.common.entity.BackResult; // 通用返回结果类
import org.springframework.security.access.prepost.PreAuthorize; // Spring Security权限注解
import org.springframework.transaction.annotation.Transactional; // Spring事务注解
import org.springframework.web.bind.annotation.*; // Spring Web注解

import javax.annotation.Resource; // javax注解
import java.util.Arrays; // Java数组工具类
import java.util.Date; // Java日期类
import java.util.List; // Java列表类
import java.util.stream.Collectors; // Java Stream流操作类

/**
 * 系统菜单Controller控制器
 */
@RestController // 表明这是一个REST控制器
@RequestMapping("/sys/menu") // 映射请求路径
public class SysMenuController {

    @Resource // 自动注入SysMenuService
    private SysMenuService sysMenuService;

    @Resource // 自动注入SysRoleMenuService
    private SysRoleMenuService sysRoleMenuService;

    @GetMapping("/treeList") // 映射GET请求到/treeList
    @PreAuthorize("hasAuthority('system:menu:query')") // 需要'system:menu:query'权限才能访问
    public BackResult treeList(){ // 获取菜单树列表
        List<SysMenu> menuList = sysMenuService.selectAllAsc(); // 查询所有菜单并按升序排序
        return BackResult.success(sysMenuService.buildTreeMenu(menuList)); // 构建菜单树并返回成功结果
    }

    /**
     * 根据角色id获取菜单id集
     * @param roleId 角色id
     * @return 菜单id集
     */
    @GetMapping("/menus/{id}") // 映射GET请求到/menus/{id}
    @PreAuthorize("hasAuthority('system:role:menu')") // 需要'system:role:menu'权限才能访问
    public BackResult menus(@PathVariable("id") Integer roleId){ // 根据角色id获取菜单id集
        List<SysRoleMenu> sysRoleMenus = sysRoleMenuService.selectByRoleId(roleId); // 根据角色id查询角色菜单
        List<Long> menuIdList = sysRoleMenus.stream() // 创建Stream流
                .map(SysRoleMenu::getMenuId) // 获取菜单id
                .collect(Collectors.toList()); // 收集到列表
        return BackResult.success(menuIdList); // 返回成功结果
    }

    /**
     * 更新角色权限信息
     * @param roleId 角色id
     * @param menuIds 菜单id数组
     * @return 更新结果
     */
    @Transactional // 开启事务
    @PostMapping("/updateMenus/{id}") // 映射POST请求到/updateMenus/{id}
    @PreAuthorize("hasAuthority('system:role:menu')") // 需要'system:role:menu'权限才能访问
    public BackResult updateMenus(@PathVariable("id")Long roleId, // 角色id
                                  @RequestBody Long[] menuIds){ // 菜单id数组

        sysRoleMenuService.deleteByRoleId(roleId); // 根据角色id删除角色菜单
        List<SysRoleMenu> roleMenuList = Arrays.stream(menuIds) // 创建Stream流
                .map(menuId -> // 映射菜单id
                        SysRoleMenu.builder() // 创建SysRoleMenu构建器
                                .roleId(roleId) // 设置角色id
                                .menuId(menuId) // 设置菜单id
                                .build() // 构建SysRoleMenu对象
                ).collect(Collectors.toList()); // 收集到列表
        sysRoleMenuService.insertBatch(roleMenuList); // 批量插入角色菜单
        return BackResult.success("更新角色权限信息成功!"); // 返回成功结果
    }

    /**
     * 更新菜单信息
     * @param sysMenu 系统菜单对象
     * @return 更新结果
     */
    @PostMapping("/save") // 映射POST请求到/save
    @PreAuthorize("hasAuthority('system:menu:edit')") // 需要'system:menu:edit'权限才能访问
    public BackResult save(@RequestBody SysMenu sysMenu){ // 更新菜单信息
        sysMenu.setUpdateTime(new Date()); // 设置更新时间
        sysMenuService.save(sysMenu); // 保存菜单
        return BackResult.success("菜单修改成功"); // 返回成功结果
    }

    /**
     * 添加菜单信息
     * @param sysMenu 系统菜单对象
     * @return 添加结果
     */
    @PostMapping("/add") // 映射POST请求到/add
    @PreAuthorize("hasAuthority('system:menu:add')") // 需要'system:menu:add'权限才能访问
    public BackResult insert(@RequestBody SysMenu sysMenu){ // 添加菜单信息
        sysMenu.setUpdateTime(new Date()); // 设置更新时间
        sysMenu.setCreateTime(new Date()); // 设置创建时间
        sysMenuService.insert(sysMenu); // 插入菜单
        return BackResult.success("菜单添加成功"); // 返回成功结果
    }

    /**
     * 根据id查询菜单具体信息
     * @param id 菜单id
     * @return 菜单信息
     */
    @GetMapping("/{id}") // 映射GET请求到/{id}
    @PreAuthorize("hasAuthority('system:menu:query')") // 需要'system:menu:query'权限才能访问
    public BackResult getMenuById(@PathVariable("id") Integer id){ // 根据id查询菜单具体信息
        return BackResult.success(sysMenuService.selectById(id)); // 返回成功结果
    }

    @PostMapping("/delete/{id}") // 映射POST请求到/delete/{id}
    public BackResult delete(@PathVariable("id") Long id){ // 删除菜单
        int cnt = sysMenuService.countByParentId(id); // 查询子菜单数量
        if(cnt>0){ // 如果存在子菜单
            return BackResult.failure(400,"请先删除子菜单"); // 返回失败结果
        }
        sysMenuService.deleteById(id); // 删除菜单
        return BackResult.success("删除该菜单成功"); // 返回成功结果
    }

}
