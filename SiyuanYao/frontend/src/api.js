import {apiUrl} from "@/ config";
import {ElMessage} from "element-plus";

function is_empty_obj(obj){
    try{
        return Object.keys(obj).length === 0;
    }catch(e){
        return false
    }
}

export class Request {
    constructor(method, url, payload = {}, contentType = 'application/json') {

        this.init = {
            method: method,
            headers: {
                'Content-Type': contentType,
            }
        }
        const token = get_token();
        if(token){
            this.init.headers['Authorization'] = token
        }
        this.url = apiUrl + url;

        if (!is_empty_obj(payload)) {
            this.init.body = JSON.stringify(payload);
        }
    }

    async go() {
        try{
            this.response = await fetch(this.url, this.init)
        }catch{
            return this.network_error()
        }

        try{
            this.data = await this.response.json()
        }catch{
            this.data = null
        }

        if (this.response.ok) {
            await this.success()
        }else{
            await this.error()
        }
    }

    show_message(){
        if(this.response.ok){
            ElMessage.success(this.data.message)
        }else{
            ElMessage.error(this.data.message);
        }
    }

    async success() {
        this.show_message()
    }

    get_data(...args){
        if(this.data === null){
            throw new Error('No data')
        }
        let data = this.data
        for(let key of args){
            data = data[key]
        }
        return data;
    }

    async error() {
        this.show_message()
    }

    network_error() {
        return ElMessage.error('网络错误')
    }
}

export function get_date_str(){
    return new Date(Date.now() + 8 * 60 * 60 * 1000).toISOString().split('T')[0]
}

export function get_token(){
    return localStorage.getItem('token');
}

/**
 * @function disabled_date
 * @param date {Date}
 * @return {boolean}
 * */
export function disabled_date(date) {
    let timestamp = date.getTime();
    if (timestamp > Date.now() + 24 * 60 * 60 * 1000) {
        return true
    }
    if (timestamp < Date.now() - 3 * 30 * 24 * 60 * 60 * 1000) {
        return true
    }
    return false
}