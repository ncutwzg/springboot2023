package com.speed.controller;

import com.speed.eneity.Account;
import com.speed.eneity.Aviation;
import com.speed.eneity.ResultDTO;
import com.speed.jwt.utils.JwtUtil;
import com.speed.service.AccountService;
import com.speed.service.AviationService;
import jakarta.annotation.Resource;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/flights")
public class AviationController {

    @Resource
    private JwtUtil jwtUtil;

    @Autowired
    private AviationService aviationService;


    @Autowired
    private AccountService accountService;

    // 获取所有航班
    @GetMapping
    public ResultDTO getData(String type, String keyword, String date) {
        List<Aviation> aviations = aviationService.getData(type,keyword,date);
        return ResultDTO.success("success",aviations);
    }

    @PostMapping
    @RequiresAuthentication
    @RequiresPermissions("manage")
    public ResultDTO insert(@RequestBody Aviation aviation) {
        if (aviation.getAirlineCn()!=null && aviation.getFlightNo()!=null && aviation.getTime()!=null) {
            aviationService.insert(aviation);
            return ResultDTO.success();
        }
        else {
            return ResultDTO.error("参数错误");
        }
    }

    @DeleteMapping
    @RequiresAuthentication
    @RequiresPermissions("manage")
    public ResultDTO delete(Integer id,@RequestHeader("Authorization") String token) {
        String username = jwtUtil.getClaimsByToken(token).getSubject();
        Account account = accountService.selectByUsername(username);
        Aviation aviation1 = aviationService.selectById(id);
        try {
            if(account.getUsername().equals(aviation1.getCreateUserName())) {
                aviationService.deleteById(id);
            } else {
                aviationService.deleteById_role(id);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ResultDTO.success("删除成功");
    }

    @PutMapping
    @RequiresAuthentication
    @RequiresPermissions("manage")
    public ResultDTO update(
            Integer id,
            @RequestBody Aviation aviation,
            @RequestHeader("Authorization") String token){
        String username = jwtUtil.getClaimsByToken(token).getSubject();
        Account account = accountService.selectByUsername(username);
        Aviation aviation1 = aviationService.selectById(id);
        try {
            if(account.getUsername().equals(aviation1.getCreateUserName())) {
                aviationService.updateById(id,aviation);
            } else {
                aviationService.updateById_role(id,aviation);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ResultDTO.success("更改成功");
    }


}
