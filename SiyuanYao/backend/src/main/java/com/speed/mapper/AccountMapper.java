package com.speed.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.speed.eneity.Account;

public interface AccountMapper extends BaseMapper<Account> {
}
