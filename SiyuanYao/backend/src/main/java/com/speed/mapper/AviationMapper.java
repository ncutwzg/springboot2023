package com.speed.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.speed.eneity.Aviation;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface AviationMapper extends BaseMapper<Aviation> {

    @Select("select * from aviation where flightNo like '%${keyword}%' and time = #{date}")
    List<Aviation> selectFlights(String keyword, String date);

    @Select("select * from aviation where park like '${keyword}%' and time = #{date}")
    List<Aviation> selectApron(String keyword, String date);

    @Select("select * from aviation where park = #{keyword} and time = #{date}")
    List<Aviation> selectPark(String keyword, String date);

}
