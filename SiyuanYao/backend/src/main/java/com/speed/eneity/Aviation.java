package com.speed.eneity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

@Data
public class Aviation implements Serializable {
    private static final  long serialVersionUID = -1L;
    @TableId(value = "id",type = IdType.INPUT)
    private Integer id;             //id

    private String airlineCn;       //航司中文名
    private String flightNo;        //航班号
    private String domint;          //航路类型：I-国际，D-国内
    private String flightType;      //航班类型
    private String taskNature;      //任务性质
    private String airType;         //机型
    private String time;            //到达时间
    private String dest;            //目的地机场，当flightStatus为A时此处为null
    private String startAirport;    //出发机场，当flightStatus为Ｄ时此处为null
    private String park;           //停机位，三到四为字符
    private String flightStatus;    //航班状态，A-到达 D-出发 C-取消
    private int unCommon;           //是否在常见,0-常见，1-航司，2-机型，两个都存在取1
    private String createUserName;  //创建用户
}
