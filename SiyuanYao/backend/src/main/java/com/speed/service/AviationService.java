package com.speed.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.speed.eneity.Account;
import com.speed.eneity.Aviation;
import com.speed.mapper.AccountMapper;
import com.speed.mapper.AviationMapper;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AviationService extends ServiceImpl<AviationMapper, Aviation> {

    @Autowired
    private AviationMapper aviationMapper;
    public List<Aviation> getData(String type,  String keyword, String date) {
        List<Aviation> aviations = new ArrayList<>();
        if(type.equals("flights")) {
            aviations = aviationMapper.selectFlights(keyword,date);
        } else if (type.equals("apron")) {
            aviations = aviationMapper.selectApron(keyword,date);
        } else if (type.equals("park")) {
            aviations = aviationMapper.selectPark(keyword,date);
        }
        return aviations;
    }

    public void insert(Aviation aviation) {
        try {
            aviationMapper.insert(aviation);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public void deleteById(Integer id) {
        try {
            aviationMapper.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @RequiresRoles("administrator")
    public void deleteById_role(Integer id) {
        try {
            aviationMapper.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Aviation selectById(Integer id) {
        try {
            QueryWrapper<Aviation> wrapper = new QueryWrapper<>();
            wrapper.eq("id",id);
            return aviationMapper.selectOne(wrapper);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public void updateById(Integer id, Aviation aviation) {
        QueryWrapper<Aviation> wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        try {
            // 更新
            aviationMapper.selectById(id);
            aviationMapper.update(aviation,wrapper);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



    @RequiresRoles("administrator")
    public void updateById_role(Integer id, Aviation aviation) {
        QueryWrapper<Aviation> wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        try {
            // 更新
            aviationMapper.selectById(id);
            aviationMapper.update(aviation,wrapper);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
