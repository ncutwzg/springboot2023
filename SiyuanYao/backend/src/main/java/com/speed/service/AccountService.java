package com.speed.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.speed.eneity.Account;
import com.speed.mapper.AccountMapper;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService extends ServiceImpl<AccountMapper, Account> {

    @Autowired
    private AccountMapper accountMapper;



    public Account selectByUsername(String username) {
        QueryWrapper<Account> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        try {
            return accountMapper.selectOne(wrapper);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
