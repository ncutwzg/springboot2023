package com.lantu.sys.service;

import com.lantu.sys.entity.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;


public interface IRoleMenuService extends IService<RoleMenu> {

}
