package com.lantu.sys.mapper;

import com.lantu.sys.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface RoleMapper extends BaseMapper<Role> {

}
