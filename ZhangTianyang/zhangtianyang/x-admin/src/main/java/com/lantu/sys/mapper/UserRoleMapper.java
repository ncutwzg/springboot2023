package com.lantu.sys.mapper;

import com.lantu.sys.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface UserRoleMapper extends BaseMapper<UserRole> {

}
