package com.lantu.sys.service.impl;

import com.lantu.sys.entity.Role;
import com.lantu.sys.mapper.RoleMapper;
import com.lantu.sys.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
