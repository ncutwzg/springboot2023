package com.lantu.sys.service.impl;

import com.lantu.sys.entity.UserRole;
import com.lantu.sys.mapper.UserRoleMapper;
import com.lantu.sys.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
