package com.lantu.sys.mapper;

import com.lantu.sys.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
