package com.lantu.sys.service;

import com.lantu.sys.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;


public interface IUserRoleService extends IService<UserRole> {

}
