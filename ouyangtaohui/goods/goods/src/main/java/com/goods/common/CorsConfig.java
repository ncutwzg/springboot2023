package com.goods.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 解决跨域问题和配置静态资源映射
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    /**
     * 配置跨域请求处理
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // 对所有的路径生效
                .allowedOriginPatterns("*") // 允许所有的来源
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS") // 允许的请求方法
                .allowCredentials(true) // 是否支持跨域用.0.户凭证
                .maxAge(3600) // 预检请求的有效期，单位秒
                .allowedHeaders("*"); // 允许的请求头
    }

    /**
     * 配置静态资源映射
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 映射路径 /images/** 到文件系统路径 D:/springboot_class/practice/Job/img/
        registry.addResourceHandler("/images/**")
                .addResourceLocations("file:D:/springboot_class/practice/Job/images/");
    }
}
