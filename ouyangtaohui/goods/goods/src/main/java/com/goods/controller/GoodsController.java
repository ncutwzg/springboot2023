package com.goods.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goods.entity.GoodsEntity;
import com.goods.service.GoodsService;
import com.goods.utils.PageUtils;
import com.goods.utils.R;

/**
 * 商品控制器
 */
@RestController
@RequestMapping("goods/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    /**
     * 商品列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("goods:goods:list")
    public R list(@RequestParam Map<String, Object> params){
        // 调用商品服务的查询方法，获取商品列表的分页信息
        PageUtils page = goodsService.queryPage(params);

        // 返回查询结果，封装为响应对象
        return R.ok().put("page", page);
    }

    /**
     * 商品信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("goods:goods:info")
    public R info(@PathVariable("id") Integer id){
        // 根据商品ID查询商品信息
        GoodsEntity goods = goodsService.getById(id);

        // 返回查询结果，封装为响应对象
        return R.ok().put("goods", goods);
    }

    /**
     * 保存商品
     */
    @RequestMapping("/save")
    //@RequiresPermissions("goods:goods:save")
    public R save(@RequestBody GoodsEntity goods){
        // 调用商品服务的保存方法
        goodsService.save(goods);

        // 返回保存成功的响应
        return R.ok();
    }

    /**
     * 修改商品
     */
    @RequestMapping("/update")
    //@RequiresPermissions("goods:goods:update")
    public R update(@RequestBody GoodsEntity goods){
        // 调用商品服务的更新方法
        goodsService.updateById(goods);

        // 返回更新成功的响应
        return R.ok();
    }

    /**
     * 删除商品
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("goods:goods:delete")
    public R delete(@RequestBody Integer[] ids){
        // 调用商品服务的批量删除方法
        goodsService.removeByIds(Arrays.asList(ids));

        // 返回删除成功的响应
        return R.ok();
    }
}
