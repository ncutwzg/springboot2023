package com.goods.utils;

import com.goods.exception.RRException;
import org.apache.commons.lang3.StringUtils;

/**
 * SQL 过滤工具类，用于防止 SQL 注入攻击
 */
public class SQLFilter {

    /**
     * SQL 注入过滤方法
     *
     * @param str 待验证的字符串
     * @return 过滤后的字符串
     */
    public static String sqlInject(String str) {
        // 如果输入字符串为空，直接返回 null
        if (StringUtils.isBlank(str)) {
            return null;
        }

        // 去除一些特殊字符
        str = StringUtils.replace(str, "'", "");
        str = StringUtils.replace(str, "\"", "");
        str = StringUtils.replace(str, ";", "");
        str = StringUtils.replace(str, "\\", "");

        // 将字符串转换为小写，增加匹配的准确性
        str = str.toLowerCase();

        // 定义一些 SQL 关键字
        String[] keywords = {"master", "truncate", "insert", "select", "delete", "update", "declare", "alter", "drop"};

        // 检查是否包含非法关键字
        for (String keyword : keywords) {
            if (str.indexOf(keyword) != -1) {
                // 如果包含非法关键字，抛出自定义异常
                throw new RRException("包含非法字符");
            }
        }

        // 返回过滤后的字符串
        return str;
    }
}
