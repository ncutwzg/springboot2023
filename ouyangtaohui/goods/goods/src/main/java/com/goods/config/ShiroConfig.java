package com.goods.config;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {
    //配置realm
    @Bean
    public Realm realm(){
        return  new LoginAuthorizingRealm();
    }
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean=new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        //过滤器规则
        Map<String,String> filterChainDefinitionMap=new LinkedHashMap<>();
        filterChainDefinitionMap.put("/goods/user/**","anon");
        filterChainDefinitionMap.put("/goods/goods/**","anon");
        filterChainDefinitionMap.put("/upload/**","anon");
        filterChainDefinitionMap.put("/admin/login","anon");
        filterChainDefinitionMap.put("/admin/unAuth","anon");
        filterChainDefinitionMap.put("/admin/401","anon");
        filterChainDefinitionMap.put("/**","authc");
        //这里定义用户未认证时跳转的路径
        shiroFilterFactoryBean.setLoginUrl("/admin/401");
        //这里是用户登录成功后跳转的路径
        //shiroFilterFactoryBean.setSuccessUrl("/admin/index");
        //这里是用户没有访问权限跳转的路径
        shiroFilterFactoryBean.setUnauthorizedUrl("/admin/unAuth");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }
    //    将realm加入管理器中
    @Bean(name="securityManager")
    public DefaultWebSecurityManager defaultWebSecurityManager(){
        DefaultWebSecurityManager defaultWebSecurityManager=new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(realm());
        return defaultWebSecurityManager;
    }


}
