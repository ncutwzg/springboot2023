package com.goods.config;

import com.goods.entity.UserEntity;
import com.goods.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
@Service
public class LoginAuthorizingRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;

    // 授权方法，用于获取用户的角色和权限信息
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) getAvailablePrincipal(principals);

        // 系统角色
        Set<String> roles = new HashSet<>();
        roles.add("超级管理员");

        // 角色所拥有的权限
        Set<String> permissions = new HashSet<>();

        // 这里定义了一个权限列表，与后面在 controller 中的注解一致即可
        if (username.equals("user")) {
            permissions.add("admin:shiro:list");
            permissions.add("goods:user:shiro:save");
            permissions.add("goods:user:shiro:update");
            permissions.add("goods:user:shiro:delete");
        }

        // 为当前用户添加角色和权限
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setRoles(roles);
        info.setStringPermissions(permissions);

        return info;
    }

    // 身份验证方法，用于验证用户的身份信息
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        // 验证账号密码是否正确，这里简单地定义了一个账号，模拟数据库中的用户
        List<UserEntity> list = userService.list();

        Map<String, String> userInfo = new HashMap<>();
        userInfo.put("admin", "123");

        // 将数据库中的用户信息存入 userInfo Map 中
        for (UserEntity user : list) {
            userInfo.put(user.getAccount(), user.getPassword());
        }

        // 强转的类型不一定要是 UsernamePasswordToken，具体要看你在登录接口中所传的对象类型
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        String username = usernamePasswordToken.getUsername();

        // 判断用户名是否为空
        if (StringUtils.isEmpty(username)) {
            throw new AccountException("用户名不能为空");
        }

        // 判断用户是否存在
        // 这里的思路是：判断数据库中是否存在该用户
        // 密码校验由 Shiro 的认证器处理
        String password = userInfo.get(username);
        if (StringUtils.isEmpty(password)) {
            throw new UnknownAccountException("用户不存在");
        }

        // 创建 SimpleAuthenticationInfo 对象，包含用户名和密码信息，由 Shiro 进行密码校验
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(username, password, username);

        return simpleAuthenticationInfo;
    }
}

