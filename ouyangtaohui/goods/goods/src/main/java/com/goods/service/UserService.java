package com.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.goods.utils.PageUtils;
import com.goods.entity.UserEntity;

import java.util.Map;

/**
 *
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

