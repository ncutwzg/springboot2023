package com.goods.dao;

import com.goods.entity.GoodsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 */
@Mapper
public interface GoodsDao extends BaseMapper<GoodsEntity> {

}
