package com.goods.service.impl;

import com.goods.entity.UserEntity;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.goods.utils.PageUtils;
import com.goods.utils.Query;

import com.goods.dao.GoodsDao;
import com.goods.entity.GoodsEntity;
import com.goods.service.GoodsService;

@Service("goodsService")
public class GoodsServiceImpl extends ServiceImpl<GoodsDao, GoodsEntity> implements GoodsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        // 使用 QueryWrapper 构建查询条件
        QueryWrapper<GoodsEntity> qw = new QueryWrapper<>();
        if (params.containsKey("goodsName") && params.get("goodsName").toString() != "") {
            // 修改查询列名为 goods_name
            qw.eq("goods_name", params.get("goodsName").toString());
        }

        // 调用 MyBatis-Plus 的 page 方法，传入自定义的 Query 对象，获取分页信息
        IPage<GoodsEntity> page = this.page(
                new Query<GoodsEntity>().getPage(params), qw
        );

        // 封装分页结果为 PageUtils 类，方便返回
        return new PageUtils(page);
    }
}
