package com.goods.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.goods.utils.PageUtils;
import com.goods.utils.Query;

import com.goods.dao.UserDao;
import com.goods.entity.UserEntity;
import com.goods.service.UserService;

@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        // 创建 QueryWrapper 以构建查询条件
        QueryWrapper<UserEntity> qw = new QueryWrapper<>();

        // 检查 "account" 参数是否存在且不为空
        if(params.containsKey("account") && params.get("account").toString()!=""){
            // 添加查询条件：列名 "account" 等于 "account" 参数的值
            qw.eq("account",params.get("account").toString());
        }

        // 检查 "password" 参数是否存在
        if(params.containsKey("password")){
            // 添加查询条件：列名 "password" 等于 "password" 参数的值
            qw.eq("password",params.get("password").toString());
        }

        // 使用 MyBatis-Plus 的 page 方法执行分页查询
        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params), // 创建自定义 Query 对象以处理分页
                qw // 应用包含条件的 QueryWrapper
        );

        // 将分页结果封装成 PageUtils 对象以便返回
        return new PageUtils(page);
    }

}
