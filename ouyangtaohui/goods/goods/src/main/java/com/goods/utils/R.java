package com.goods.utils;

import java.util.HashMap;
import java.util.Map;

/**
 返回数据的工具类
 */
public class R extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;

	/**
	 * 构造函数，默认设置 code 为 0
	 */
	public R() {
		put("code", 0);
	}

	/**
	 * 返回一个表示未知异常的 R 对象，默认 code 为 500，msg 为 "未知异常，请联系管理员"
	 */
	public static R error() {
		return error(500, "未知异常，请联系管理员");
	}

	/**
	 * 返回一个表示异常的 R 对象，指定异常信息
	 * @param msg 异常信息
	 */
	public static R error(String msg) {
		return error(500, msg);
	}

	/**
	 * 返回一个表示异常的 R 对象，指定异常 code 和 msg
	 * @param code 异常代码
	 * @param msg 异常信息
	 */
	public static R error(int code, String msg) {
		R r = new R();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}

	/**
	 * 返回一个表示成功的 R 对象，指定成功信息
	 * @param msg 成功信息
	 */
	public static R ok(String msg) {
		R r = new R();
		r.put("msg", msg);
		return r;
	}

	/**
	 * 返回一个表示成功的 R 对象，指定数据集合
	 * @param map 数据集合
	 */
	public static R ok(Map<String, Object> map) {
		R r = new R();
		r.putAll(map);
		return r;
	}

	/**
	 * 返回一个表示成功的 R 对象，默认没有附加信息
	 */
	public static R ok() {
		return new R();
	}

	/**
	 * 添加键值对到 R 对象
	 * @param key 键
	 * @param value 值
	 * @return 返回当前 R 对象，用于链式调用
	 */
	public R put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
