package com.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.goods.utils.PageUtils;
import com.goods.entity.GoodsEntity;

import java.util.Map;

/**
 *
 */
public interface GoodsService extends IService<GoodsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

