package com.goods.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goods.entity.UserEntity;
import com.goods.service.UserService;
import com.goods.utils.PageUtils;
import com.goods.utils.R;



@RestController
@RequestMapping("goods/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("goods:user:list")
    public R list(@RequestParam Map<String, Object> params) {
        try {
            // 获取当前用户的 Subject 对象
            Subject subject = SecurityUtils.getSubject();
            if (params.containsKey("account") && params.get("account").toString() != "") {
                // 从请求参数中获取账号和密码
                String username = params.get("account").toString();
                String password = params.get("password").toString();
                // 使用 Shiro 进行登录操作
                subject.login(new UsernamePasswordToken(username, password));
            }
            // 查询用户列表
            PageUtils page = userService.queryPage(params);
            // 返回查询结果的响应
            return R.ok().put("page", page);
        } catch (UnknownAccountException e) {
            // 账号不存在的异常处理
            return R.ok().put("page", "账号不存在");
        } catch (AccountException e) {
            // 账号或密码不正确的异常处理
            return R.ok().put("page", "账号或密码不正确");
        } catch (IncorrectCredentialsException e) {
            // 密码不正确的异常处理
            return R.ok().put("page", "账号或密码不正确");
        }
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("goods:user:info")
    public R info(@PathVariable("id") Integer id) {
        // 根据用户 ID 获取用户信息
        UserEntity user = userService.getById(id);
        // 返回用户信息的响应
        return R.ok().put("user", user);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("goods:user:shiro:save")
    public R save(@RequestBody UserEntity user) {
        // 保存用户信息
        userService.save(user);
        // 返回保存成功的响应
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("goods:user:update")
    @RequiresPermissions("goods:user:shiro:update")
    public R update(@RequestBody UserEntity user) {
        // 根据用户 ID 更新用户信息
        userService.updateById(user);
        // 返回更新成功的响应
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("goods:user:delete")
    @RequiresPermissions("goods:user:shiro:delete")
    public R delete(@RequestBody Integer[] ids) {
        // 根据用户 ID 批量删除用户
        userService.removeByIds(Arrays.asList(ids));
        // 返回删除成功的响应
        return R.ok();
    }
}
