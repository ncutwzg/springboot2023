import Vue from 'vue'
import VueRouter from 'vue-router'

import HelloWorld from '@/components/HelloWorld'//测试页面


import Login from '@/components/Login'
import regist from '@/components/regist'
import Home from '@/components/Home'
import Welcome from '@/components/Welcome'
import Main from '@/components/Main'

import goods from '@/components/views/goods'
import user from '@/components/views/user'

Vue.use(VueRouter) 
const router= new VueRouter({
//   mode: 'hash',
  routes:[
      { path: '/', redirect: '/login' },
      { path: '/login', component: Login },
      { path: '/regist', component: regist },
      {
          path:'/Home',
          name:'Home',
          component:Home,
          redirect:'/goods',//访问home即可重定向到welcome界面
          children:[
              {
              path:'/goods',name:'goods',component:goods
              },
              {
                path:'/user',name:'user',component:user
              },
          ]
      }
  ]
});

export default router;
