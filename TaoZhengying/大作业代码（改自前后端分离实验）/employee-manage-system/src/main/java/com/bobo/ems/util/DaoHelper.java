package com.bobo.ems.util;

import com.bobo.ems.entity.Employee;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class DaoHelper {
    private static List<Employee> employeeList = new ArrayList();
    public static final String url = "jdbc:mysql://localhost:3306/test"; // 本地student数据库地址

    private final static String username = "root";// MySQL数据库登录用户名

    private final static String password = "123456";// MySQL数据库登录密码



    public static List<Employee> getEmployeeList() {
        employeeList.clear();
        try{
            try {

                Class.forName("com.mysql.cj.jdbc.Driver");// 加载MySQL数据库驱动

            } catch (Exception e) {

                e.printStackTrace();

                System.out.println("未能成功加载数据库驱动程序！");

            }

            try {

                Connection connect = DriverManager.getConnection(url, username, password);

                Statement stmt = connect.createStatement();

                // 增加字段

                // stmt.executeUpdate("select * from S;");


                ResultSet rs = stmt.executeQuery("select * from employee;");

                while (rs.next()) {
                    Employee e = new Employee();
                    e.setId(rs.getInt("id"));
                    e.setBirthday(rs.getDate("birthday"));
                    e.setDepartment(rs.getString("department"));
                    e.setName(rs.getString("name"));
                    e.setNumber(rs.getInt("number"));
                    e.setPosition(rs.getString("position"));
                    e.setSalary(rs.getInt("salary"));
                    e.setSex(rs.getString("sex"));
                    employeeList.add(e);
                }
                connect.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return employeeList;
    }
    public static Employee getEmployeeById(Integer id){
        return employeeList.stream().collect(Collectors.toMap(Employee::getId, e->e)).get(id);
    }
    public static Employee deleteEmployeeById(Integer id){
        try{
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");// 加载MySQL数据库驱动
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("未能成功加载数据库驱动程序！");
            }
            try {
                Connection connect1 = DriverManager.getConnection(url, username, password);
                Statement stmt1 = connect1.createStatement();
                stmt1.executeUpdate("delete from employee where id = " + id);
                connect1.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static Employee updateEmployee(Employee employee){
        Employee oldEmployee = employeeList.stream().collect(
                Collectors.toMap(Employee::getId, e -> e)).get(employee.getId());
//        if(employee.getName() != null){
//            oldEmployee.setName(employee.getName());
//        }
//        if(employee.getNumber() != null){
//            oldEmployee.setNumber(employee.getNumber());
//        }
//        if(employee.getDepartment() != null){
//            oldEmployee.setDepartment(employee.getDepartment());
//        }
//        if(employee.getPosition() != null){
//            oldEmployee.setPosition(employee.getPosition());
//        }
//        if(employee.getSex() != null){
//            oldEmployee.setSex(employee.getSex());
//        }
//        if(employee.getSalary() != null){
//            oldEmployee.setSalary(employee.getSalary());
//        }
//        if(employee.getBirthday() != null){
//            oldEmployee.setBirthday(employee.getBirthday());
//        }
        return oldEmployee;
    }

    public static Employee insertEmployee(Employee employee){
//        employeeList.add(employee);
        return employee;
    }
}
