import Vue from 'vue'
import Router from 'vue-router'
import employee from '@/components/employee'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/employee', // 设置默认指向的路径
      name: 'default'
    },
    {
      path: '/employee',
      name: 'employee',
      component: employee
    }
  ]
})
