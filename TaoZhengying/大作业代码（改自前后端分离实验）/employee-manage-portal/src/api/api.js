import axios from 'axios'
let base = '/ems'
axios.defaults.baseURL = 'http://localhost:8080'
axios.defaults.timeout = 10000

export const listEmployee = params => { return axios.get(`${base}/employees`) }
export const deleteEmployee = id => { return axios.delete(`${base}/employees/${id}`); }