package cn.edu.ncut.cs.springboot.principleautoconfiguration.config;

import cn.edu.ncut.cs.springboot.principleautoconfiguration.bean.Pet;
import cn.edu.ncut.cs.springboot.principleautoconfiguration.bean.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration //告诉SpringBoot 这是一个配置类，等价于一个xml配置文件
public class MyConfig {
    @Bean //给容器中添加组件，以方法名作为组件的id，返回类型为组件类型，返回值就是组件在容器中的示例
    public User myConfigBeanUser() {
        return new User("zhangsan", 18);
    }

    @Bean
    public Pet myConfigBeanPet() {
        return new Pet("tomcat");
    }
}
