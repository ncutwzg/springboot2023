package com.guodexian.testkaptchaautoconfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestKaptchaAutoConfigurationApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestKaptchaAutoConfigurationApplication.class, args);
	}

}
