package com.tudou.springbootmybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tudou.springbootmybatis.entity.Student;

public interface StudentMapper extends BaseMapper<Student> {
}