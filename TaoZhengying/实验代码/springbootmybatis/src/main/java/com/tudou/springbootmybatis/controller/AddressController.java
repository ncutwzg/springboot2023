package com.tudou.springbootmybatis.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tudou.springbootmybatis.entity.Address;
import com.tudou.springbootmybatis.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzhigang
 * @since 2023-10-07
 */
@RestController
@RequestMapping("/address")
public class AddressController {
    @Autowired
    private IAddressService addressService;

    @PutMapping("/saveOrUpdate")
    public boolean saveOrUpdate(@RequestBody Address address){
        return addressService.saveOrUpdate(address);
    }
    @GetMapping("/list")
    public List<Address> remove(){
        return addressService.list();
    }
}