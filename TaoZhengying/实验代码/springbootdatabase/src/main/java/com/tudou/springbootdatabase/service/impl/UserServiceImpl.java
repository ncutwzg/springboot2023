package com.tudou.springbootdatabase.service.impl;

import com.tudou.springbootdatabase.dao.UserDao;
import com.tudou.springbootdatabase.entity.User;
import com.tudou.springbootdatabase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public Optional<User> findById(Integer id) {
        return userDao.findById(id);
    }

    @Override
    public void saveUser(User user) {
        userDao.save(user);
    }

    @Override
    public void delete(Integer id) {
        User user = new User();
        user.setId(id);
        userDao.delete(user);
    }
}
