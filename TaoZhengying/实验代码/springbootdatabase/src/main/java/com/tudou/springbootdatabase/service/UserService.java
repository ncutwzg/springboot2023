package com.tudou.springbootdatabase.service;

import com.tudou.springbootdatabase.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> findAll();

    Optional<User> findById(Integer id);

    void saveUser(User user);

    void delete(Integer id);
}
