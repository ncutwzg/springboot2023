package org.mrchi;

import org.mrchi.aop.Calculator;
import org.mrchi.aspect.LogAspects;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy//用于启动Spring的注解Aspect功能
public class AOPBeanConfig {
    @Bean
    public Calculator calculator() {
        return new Calculator();
    }
    @Bean
    public LogAspects logAspects() {
        return new LogAspects();
    }
}
