insert into user(id, username, password, salt, gmt_create, gmt_modified)
select
    NULL, 'admin','$2a$10$SljMzzv/fAxZEBjv3ckLoOgTfZYp82Br/ZHZRt4E6tM465HJR9wG.', '', now(), now()
from dual
where not exists(
    select 1 from user where username = 'admin'
);

insert into permission(id, name, gmt_create, gmt_modified)
select
    NULL, 'user:list', now(), now()
from dual
where not exists(
    select 1 from permission where name='user:list'
);

insert into role(id, name, gmt_create, gmt_modified)
select
    NULL, 'system', now(), now()
from dual
where not exists(
    select 1 from role where name='system'
);

with t as (select
               u.id as user_id, r.id as role_id
           from user u, role r
           where u.username='admin' and r.name='system')
insert into user_role(id, user_id, role_id, gmt_create, gmt_modified)
select
    NULL, user_id, role_id, now(), now()
from t
where not exists(
    select 1 from user_role ur
                      join t
    where ur.user_id = t.user_id and ur.role_id = t.role_id
);


with t as (select
               r.id as role_id, p.id as permission_id
           from permission p, role r
           where p.name='user:list' and r.name='system')
insert into role_permission(id, role_id, permission_id, gmt_create, gmt_modified)
select
    NULL, role_id, permission_id, now(), now()
from t
where not exists(
    select 1 from role_permission rp
                      join t
    where rp.role_id = t.role_id and rp.permission_id = t.permission_id
);

commit;