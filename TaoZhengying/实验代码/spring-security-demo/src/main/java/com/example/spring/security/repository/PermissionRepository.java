package com.example.spring.security.repository;

import com.example.spring.security.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    @Query("select p from Permission p join UserRole ur on p.id = ur.roleId where ur.roleId=:roleId")
    List<Permission> getPermissions(@Param("roleId") Long roleId);
}
