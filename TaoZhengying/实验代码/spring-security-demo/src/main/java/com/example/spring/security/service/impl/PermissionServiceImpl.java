package com.example.spring.security.service.impl;

import com.example.spring.security.entity.Permission;
import com.example.spring.security.repository.PermissionRepository;
import com.example.spring.security.service.PermissionService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {
    @Resource
    private PermissionRepository permissionRepository;

    @Override
    public List<Permission> getPermissions(@NonNull Long roleId) {
        return permissionRepository.getPermissions(roleId);
    }
}