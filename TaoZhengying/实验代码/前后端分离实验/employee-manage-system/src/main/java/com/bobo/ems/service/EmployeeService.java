package com.bobo.ems.service;

import com.bobo.ems.entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> list();

    Employee getById(Integer id);

    Employee updateEmployee(Employee employee);

    Employee saveEmployee(Employee employee);

    Employee removeEmployee(Integer id);
}