package org.example.reflection;

public class Test02 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class c1 = Class.forName("org.example.reflection.User");
        System.out.println(c1);
    }
}


class User {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private String name;
    private int id;
    private int age;

    public User(String name, int id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }
}
