package org.example.reflection;

public class Test03 {
    public static void main(String[] args) throws ClassNotFoundException {
        Person person = new Student();
        System.out.println("这个人是:" + person.name);

        Class<? extends Person> c1 = person.getClass();
        System.out.println(c1.hashCode());

        Class<?> c2 = Class.forName("org.example.reflection.Student");
        System.out.println(c2.hashCode());

        Class<Student> c3 = Student.class;
        System.out.println(c3.hashCode());

        Class<Integer> c4 = Integer.TYPE;
        System.out.println(c4);

        Class<?> c5 = c1.getSuperclass();
        System.out.println(c5.hashCode());
    }
}

class Person{
    public String name;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }

    public Person(String name) {
        this.name = name;
    }

    public Person() {
    }
}

class Student extends Person{
    public Student() {
        this.name = "学生";
    }
}

class Teacher extends Person {
    public Teacher() {
        this.name = "老师";
    }
}