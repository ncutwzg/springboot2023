import java.io.*;

public class ReadFile {
    public static void main(String[] args) {
        try {
            System.out.println("file.encoding="+System.getProperty("file.encoding"));
            // 指定使用UTF-8编码读取文件
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("employee.csv"), "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

