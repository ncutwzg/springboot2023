import chardet

def get_encoding(file):
    with open(file, 'rb') as f:
        result = chardet.detect(f.read())
        return result['encoding']

filename = 'employee.csv'
encoding = get_encoding(filename)
print(f'The encoding of {filename} is {encoding}')
