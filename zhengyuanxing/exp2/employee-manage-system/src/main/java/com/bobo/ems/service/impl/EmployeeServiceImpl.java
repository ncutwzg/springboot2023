package com.bobo.ems.service.impl;

import com.bobo.ems.entity.Employee;
import com.bobo.ems.service.EmployeeService;
import com.bobo.ems.util.DaoHelper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Override
    public List<Employee> list() {
        return DaoHelper.getEmployeeList();
    }

    @Override
    public Employee getById(Integer id) {
        return DaoHelper.getEmployeeById(id);
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        return DaoHelper.updateEmployee(employee);
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return DaoHelper.insertEmployee(employee);
    }

    @Override
    public Employee removeEmployee(Integer id) {
        return DaoHelper.deleteEmployeeById(id);
    }
}
