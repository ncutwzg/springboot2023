package com.tudou.springbootfilterinterceptorlistener.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

        System.out.println("LoginInterceptor-执行了preHandle()方法！");
        request.getSession().setAttribute("username", "bobo");
        String username = (String) request.getSession().getAttribute("username");
        if (username == null) {
            response.sendRedirect("/html/login.html");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        System.out.println("LoginInterceptor-执行了postHandle()方法！");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) {
        System.out.println("LoginInterceptor-执行了afterCompletion()方法！");
    }

}
