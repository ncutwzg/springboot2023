package com.example.spring.security.controller;

import com.example.spring.security.entity.User;
import com.example.spring.security.service.UserService;
import com.example.spring.security.vo.JsonResult;
import com.example.spring.security.vo.UserVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserRestController {
    @Resource
    private UserService userService;

    //限制只有system角色的用户才可以访问
    @PreAuthorize("hasRole('system1')")
    @GetMapping("/view/{id}")
    public JsonResult getUserInfo(@PathVariable("id") Long id) {
        User user = userService.getById(id);
        UserVO userVO = new UserVO();
        userVO.setId(user.getId());
        userVO.setUsername(user.getUsername());
        userVO.setPassword(user.getPassword());
        userVO.setTimestamp(user.getGmtModified().toEpochMilli());
        return new JsonResult(JsonResult.JsonResultCode.SUCCESS, "获取数据成功！", userVO);
    }

    //限制必须拥有user:list权限的用户才可访问
    @PreAuthorize("hasAuthority('user:list')")
    @GetMapping("/list")
    public JsonResult getUserList() {
        try {
            List<UserVO> userList = userService.findAll();
            return new JsonResult(JsonResult.JsonResultCode.SUCCESS, "获取数据成功！", userList);
        } catch (Exception e) {
            e.printStackTrace();
            return new JsonResult(JsonResult.JsonResultCode.ERROR, "获取数据失败！", null);
        }
    }
}
