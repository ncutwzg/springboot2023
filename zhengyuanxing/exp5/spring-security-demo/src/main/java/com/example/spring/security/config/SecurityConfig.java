package com.example.spring.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Resource
    private UserDetailsService userDetailsService;

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 使用自定义实现的userDetailsServiceImpl来加载数据库中的用户和权限信息
        auth.userDetailsService(userDetailsService)
                // 设置加密方式，这里要和注册、创建用户时的密码加密方式使用同一个PasswordEncoder
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //开启登录配置
                .authorizeRequests()
                //设置不需要认证的路径，一律允许通过，这种方式会走Spring Security的过滤器链，当遇到这些访问路径时直接允许通过
                .antMatchers("/index", "/home").permitAll()
//                //设置需要角色的路径，表示访问 /user/view 这个接口，需要具备 system 这个角色
//                .antMatchers("/user/view/**").hasRole("system")
//                //设置需要权限许可的路径，表示访问 /user/list 这个接口，需要具有“user:list”权限许可
//                .antMatchers("/user/list").hasAuthority("user:list")
                //设置其他路径都需要登录后才可访问
                .anyRequest().authenticated()
                .and()
                //设置登录表单
                .formLogin()
                //和表单登录相关的接口直接允许通过
                .permitAll()
                //开启一个新的设置项
                .and()
                //设置登出（注销登录）
                .logout()
                //设置登出路径
                .logoutUrl("/logout")
                //设置登出成功后的处理器
                .logoutSuccessHandler(new LogoutSuccessHandler() {
                    @Override
                    public void onLogoutSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication authentication) throws IOException, ServletException {
                        resp.setContentType("application/json;charset=utf-8");
                        PrintWriter out = resp.getWriter();
                        out.write("logout success");
                        out.flush();
                    }
                })
                //和登出相关的接口允许通过
                .permitAll()
                .and()
                .csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        //设置不需要认证的路径，这种方式不走Spring Security过滤器链，推荐使用这种方式
        web.ignoring().antMatchers("/verifycode");
    }
}