CREATE TABLE `staff` (
`id` INT AUTO_INCREMENT PRIMARY KEY,
`numebr` varchar(255),
`name` varchar(255),
`department` varchar(255),
`position` varchar(255),
`birthday` DATE,
`sex` varchar(10),
`salary` INT
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
-- 向表中增添数据
INSERT INTO `staff` VALUES
(1, '10001', '张三', '产品部', '中级开发工程师', '1995-05-19', '男', 15000),
(2, '10002', '李四', '产品部', '测试工程师', '1995-08-25', '女', 12000),
(3,	'10003', '王五', '产品部', '开发实习生', '2000-11-02', '男', 5000),
(4,	'10004', '张六', '产品部', '开发实习生', '2001-11-12', '男', 5000),
(5,	'10005', '李七', '产品部', '中级开发工程师', '1983-04-11', '男', 15000),
(6,	'10006', '王九', '产品部', '中级开发工程师', '1984-07-11', '女', 15010),
(7,	'10006', '张十', '产品部', '测试工程师', '1996-02-09', '男', 12000)