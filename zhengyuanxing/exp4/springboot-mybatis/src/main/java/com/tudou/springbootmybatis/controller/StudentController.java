package com.tudou.springbootmybatis.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wuzhigang
 * @since 2023-10-06
 */
@Controller
@RequestMapping("/student")
public class StudentController {

}

