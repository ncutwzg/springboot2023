package com.tudou.springbootmybatis.mapper;

import com.tudou.springbootmybatis.entity.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuzhigang
 * @since 2023-10-07
 */
public interface AddressMapper extends BaseMapper<Address> {

}
