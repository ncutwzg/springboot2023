package com.tudou.springbootmybatis.service.impl;

import com.tudou.springbootmybatis.entity.Address;
import com.tudou.springbootmybatis.mapper.AddressMapper;
import com.tudou.springbootmybatis.service.IAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzhigang
 * @since 2023-10-07
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements IAddressService {

}
