package com.tudou.springbootmybatis.service.impl;

import com.tudou.springbootmybatis.entity.Student;
import com.tudou.springbootmybatis.mapper.StudentMapper;
import com.tudou.springbootmybatis.service.IStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzhigang
 * @since 2023-10-06
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

}
