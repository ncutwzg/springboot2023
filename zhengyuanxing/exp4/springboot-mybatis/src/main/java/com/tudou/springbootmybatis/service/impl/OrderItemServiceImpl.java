package com.tudou.springbootmybatis.service.impl;

import com.tudou.springbootmybatis.entity.OrderItem;
import com.tudou.springbootmybatis.mapper.OrderItemMapper;
import com.tudou.springbootmybatis.service.IOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuzhigang
 * @since 2023-10-07
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

}
