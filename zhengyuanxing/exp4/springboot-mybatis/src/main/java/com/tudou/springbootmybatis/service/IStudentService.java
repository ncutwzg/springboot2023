package com.tudou.springbootmybatis.service;

import com.tudou.springbootmybatis.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuzhigang
 * @since 2023-10-06
 */
public interface IStudentService extends IService<Student> {

}
