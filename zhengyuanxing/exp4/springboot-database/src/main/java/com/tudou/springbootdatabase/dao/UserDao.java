package com.tudou.springbootdatabase.dao;

import com.tudou.springbootdatabase.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Integer> {
}
