package com.example.springboothelloworldideace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHelloWorldIdeaCeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHelloWorldIdeaCeApplication.class, args);
	}

}
