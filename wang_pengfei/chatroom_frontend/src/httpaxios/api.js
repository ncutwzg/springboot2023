import dealHttp from '../httpaxios/config'

export function login( userLoginData ){
    return dealHttp({
        url: '/api/login',
        method: 'post',
        data: userLoginData
    })
}

export function register( userRegisterData ){
    return dealHttp({
        url: '/api/register',
        method: 'post',
        data: userRegisterData
    })
}

export function deleteUser( userId ){
    return dealHttp({
        url: '/api/delete_user/' + userId,
        method: 'delete'
    })
}

export function getUserList(){
    return dealHttp({
        url: '/api/user_manage',
        method: 'get'
    })
}