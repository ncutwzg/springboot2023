import axios from "axios"

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8080',
    timeout: 5000
})

axiosInstance.interceptors.request.use( (config)=>{
    config.headers.Authorization = localStorage.getItem('Authorization')
    return config
} ), (error)=>{
    console.log('Request error:', error)
    return Promise.reject(error)
}

export default axiosInstance
