import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Chat from '../views/Chat.vue'
import Login from '../views/Login.vue'
import UserManage from '../views/UserManage.vue'
import ErrorPage from '../views/ErrorPage.vue'
import Register from '../views/Register.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/chat',
      name: 'chat',
      component: Chat
    },
    {
      path: '/user_manage',
      name: 'user_manage',
      component: UserManage
    },
    {
      path: "/:catchAll(.*)",
      name: 'errorpage',
      component: ErrorPage
    }
  ]
})

router.beforeEach((to, from, next) => {
  if( (to.name == 'login') || (to.name == 'register') || (to.name == 'errorpage') ){
    next()
  }else{
    let token = localStorage.getItem('Authorization')
    if( !token ){
      next( {name: 'login'} )
    }else{
      next()
    }
  }
})

export default router
