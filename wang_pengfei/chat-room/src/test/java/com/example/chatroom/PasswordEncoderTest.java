package com.example.chatroom;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderTest {
    @Test
    public void encodePwd(){
        String password = "admin1_123";

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String encryptedPassword = passwordEncoder.encode(password);

        System.out.println(passwordEncoder.matches(password, encryptedPassword));
        System.out.println(encryptedPassword);
        // $2a$10$oVwd96FlzCQ6W1e.e.uoEuOQ2oxhLmQnqwaAuUjOSi9WQswXDzVUK
    }
}
