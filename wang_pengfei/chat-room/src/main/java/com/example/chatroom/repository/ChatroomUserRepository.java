package com.example.chatroom.repository;

import com.example.chatroom.entity.ChatroomUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatroomUserRepository extends JpaRepository<ChatroomUser, Long> {
    List<ChatroomUser> getByUsername(@NonNull String username);
}
