package com.example.chatroom.service.impl;

import com.example.chatroom.entity.Role;
import com.example.chatroom.repository.RoleRepository;
import com.example.chatroom.service.RoleService;
import jakarta.annotation.Resource;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleRepository roleRepository;

    @Override
    public List<Role> getRoles(@NonNull Long userId) {
        return roleRepository.getRoles(userId);
    }
}
