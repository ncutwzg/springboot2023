package com.example.chatroom.service;

import com.example.chatroom.entity.Permission;
import org.springframework.lang.NonNull;

import java.util.List;

public interface PermissionService {
    List<Permission> getPermissions(@NonNull Long roleId);
}
