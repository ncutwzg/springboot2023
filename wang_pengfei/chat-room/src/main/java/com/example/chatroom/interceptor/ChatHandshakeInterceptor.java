package com.example.chatroom.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

@Component
public class ChatHandshakeInterceptor extends HttpSessionHandshakeInterceptor {
}
