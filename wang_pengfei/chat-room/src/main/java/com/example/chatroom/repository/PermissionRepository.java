package com.example.chatroom.repository;

import com.example.chatroom.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    @Query( "select p " +
            "from Permission p join RolePermission rp on p.id = rp.permissionId " +
            "where rp.roleId=:roleId" )
    List<Permission> getPermissions(@Param("roleId") Long roleId);
}
