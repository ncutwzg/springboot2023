package com.example.chatroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatRoomApplication {
	// 管理员账号: [admin1, admin1_123]
	public static void main(String[] args) {
		SpringApplication.run(ChatRoomApplication.class, args);
	}

}
