package com.example.chatroom.service.impl;

import com.example.chatroom.entity.ChatroomUser;
import com.example.chatroom.entity.Permission;
import com.example.chatroom.entity.Role;
import com.example.chatroom.service.ChatroomUserService;
import com.example.chatroom.service.PermissionService;
import com.example.chatroom.service.RoleService;
import jakarta.annotation.Resource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ChatroomUserDetailsServiceImpl implements UserDetailsService {
    @Resource
    private ChatroomUserService chatroomUserService;

    @Resource
    private RoleService roleService;

    @Resource
    private PermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<ChatroomUser> chatroomUser = chatroomUserService.getByUsername(username);

        return chatroomUser.map(crU -> {
            List<Role> roles = roleService.getRoles(crU.getId());
            List<Permission> permissions = roles
                                          .stream()
                                          .flatMap(role -> permissionService.getPermissions(role.getId()).stream())
                                          .collect(Collectors.toList());

            List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles.size() + permissions.size());

            List<SimpleGrantedAuthority> roleAuthorities = roles
                                                          .stream()
                                                          .map(role -> "ROLE_" + role.getName())
                                                          .map(SimpleGrantedAuthority::new)
                                                          .collect(Collectors.toList());

            List<SimpleGrantedAuthority> permissionAuthorities = permissions
                                                                .stream()
                                                                .map(Permission::getName)
                                                                .map(SimpleGrantedAuthority::new)
                                                                .collect(Collectors.toList());

            grantedAuthorities.addAll(roleAuthorities);
            grantedAuthorities.addAll(permissionAuthorities);

            UserDetails userDetails = org.springframework.security.core.userdetails
                                     .User
                                     .builder()
                                     .username(crU.getUsername())
                                     .password(crU.getPassword())
                                     .authorities(grantedAuthorities)
                                     .build();

            return userDetails;
        }).orElse(null);
    }
}
