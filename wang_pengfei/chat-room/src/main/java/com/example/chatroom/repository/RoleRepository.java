package com.example.chatroom.repository;

import com.example.chatroom.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query( "select r " +
            "from Role r join UserRole ur on r.id = ur.roleId " +
            "where ur.userId=:userId" )
    List<Role> getRoles(@Param("userId") Long userId);
}
