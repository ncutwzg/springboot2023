package com.example.chatroom.handler;

import com.example.chatroom.msg.ChatMessage;
import com.example.chatroom.msg.ChatText;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ChatHandler extends TextWebSocketHandler {
    @Autowired
    ObjectMapper objectMapper;

    private Map<String, WebSocketSession> clients = new ConcurrentHashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("afterConnectionEstablished called");
        clients.put(session.getId(), session);
        System.out.println("afterConnectionEstablished return");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        clients.remove(session.getId());
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        System.out.println("handleTextMessage called");
        String s = message.getPayload().strip();
        if (s.isEmpty()){
            return;
        }
        ChatText chatTxt = objectMapper.readValue(s, ChatText.class);
        broadcastMsg(new ChatMessage(chatTxt.getName(), chatTxt.getText()));
        System.out.println("handleTextMessage return");
    }

    private void broadcastMsg(ChatMessage chatMsg) throws IOException {
        System.out.println("broadcastMsg called");
        TextMessage txtMsg = new TextMessage(objectMapper.writeValueAsString(chatMsg));

        for (String id : clients.keySet()){
            WebSocketSession webSocketSession = clients.get(id);
            webSocketSession.sendMessage(txtMsg);
        }
        System.out.println("broadcastMsg return");
    }
}
