package com.example.chatroom.msg;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatMessage extends ChatText{
    public ChatMessage(String name, String text){
        this.timestamp = System.currentTimeMillis();
        this.name = name;
        this.text = text;
    }

    private long timestamp;
}
