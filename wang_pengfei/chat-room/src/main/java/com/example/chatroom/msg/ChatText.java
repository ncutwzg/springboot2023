package com.example.chatroom.msg;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChatText {
    protected String name;
    protected String text;
}
