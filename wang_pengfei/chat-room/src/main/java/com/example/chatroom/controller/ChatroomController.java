package com.example.chatroom.controller;

import com.example.chatroom.entity.ChatroomUser;
import com.example.chatroom.service.ChatroomUserService;
import com.example.chatroom.vo.ChatroomUserVO;
import com.example.chatroom.vo.JsonResult;
import com.example.chatroom.vo.LoginAndRegParam;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ChatroomController {
    @Resource
    private ChatroomUserService chatroomUserService;

    @PostMapping("/login")
    public JsonResult login(@RequestBody LoginAndRegParam loginAndRegParam){
        return chatroomUserService.login(loginAndRegParam);
    }

    @PostMapping("/register")
    public JsonResult register(@RequestBody LoginAndRegParam loginAndRegParam){
        ChatroomUserVO chatroomUserVO = new ChatroomUserVO();
        chatroomUserVO.setUsername(loginAndRegParam.getUsername());
        chatroomUserVO.setPassword(loginAndRegParam.getPassword());

        if(chatroomUserService.create(chatroomUserVO)){
            return new JsonResult(JsonResult.JsonResultCode.SUCCESS, "Register success.", null);
        }
        return new JsonResult(JsonResult.JsonResultCode.ERROR, "Register failed.", null);
    }

    @GetMapping("/user_manage")
    public JsonResult userManage(){
        try {
            List<ChatroomUserVO> chatroomUserVOList = chatroomUserService.findAll();
            return new JsonResult(JsonResult.JsonResultCode.SUCCESS, "Get data success.", chatroomUserVOList);
        }catch (Exception ee){
            ee.printStackTrace();
            return new JsonResult(JsonResult.JsonResultCode.ERROR, "Get data failed.", null);
        }
    }

    @DeleteMapping("/delete_user/{id}")
    public JsonResult deleteUser(@PathVariable("id") Long id){
        if( chatroomUserService.delete(id) ){
            return new JsonResult(JsonResult.JsonResultCode.SUCCESS, "Delete user successfully.", null);
        }

        return new JsonResult(JsonResult.JsonResultCode.ERROR, "Delete user failed.", null);
    }
}
