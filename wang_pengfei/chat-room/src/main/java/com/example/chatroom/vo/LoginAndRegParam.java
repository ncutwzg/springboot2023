package com.example.chatroom.vo;

import lombok.Data;

@Data
public class LoginAndRegParam {
    private String username;

    private String password;
}
