package com.example.chatroom.service;

import com.example.chatroom.entity.Role;
import org.springframework.lang.NonNull;

import java.util.List;

public interface RoleService {
    List<Role> getRoles(@NonNull Long userId);
}
