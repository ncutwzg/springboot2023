package com.example.chatroom.service.impl;

import com.example.chatroom.entity.Permission;
import com.example.chatroom.repository.PermissionRepository;
import com.example.chatroom.service.PermissionService;
import jakarta.annotation.Resource;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {
    @Resource
    private PermissionRepository permissionRepository;

    @Override
    public List<Permission> getPermissions(@NonNull Long roleId) {
        return permissionRepository.getPermissions(roleId);
    }
}
