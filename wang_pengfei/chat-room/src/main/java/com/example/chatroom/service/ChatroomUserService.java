package com.example.chatroom.service;

import com.example.chatroom.entity.ChatroomUser;
import com.example.chatroom.vo.ChatroomUserVO;
import com.example.chatroom.vo.JsonResult;
import com.example.chatroom.vo.LoginAndRegParam;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

public interface ChatroomUserService {
    List<ChatroomUserVO> findAll();

    Optional<ChatroomUser> getByUsername(@NonNull String username);

    boolean create(@NonNull ChatroomUserVO chatroomUserVO);

    boolean delete(@NonNull Long id);

    ChatroomUser getById(@NonNull Long id);

    JsonResult login(LoginAndRegParam loginAndRegParam);
}
