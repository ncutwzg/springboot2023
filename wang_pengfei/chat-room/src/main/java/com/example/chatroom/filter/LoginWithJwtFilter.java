package com.example.chatroom.filter;

import com.example.chatroom.service.ChatroomUserService;
import com.example.chatroom.service.impl.ChatroomUserDetailsServiceImpl;
import com.example.chatroom.util.JwtUtil;
import io.jsonwebtoken.Claims;
import jakarta.annotation.Resource;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class LoginWithJwtFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtil jwtUtil;

    @Resource
    private ChatroomUserDetailsServiceImpl chatroomUserDetailsServiceImpl;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Claims claims = jwtUtil.parse((String) request.getHeader("Authorization"));

        System.out.println("Authorization in header:"+request.getHeader("Authorization"));
        if( claims==null ){
            System.out.println("Claims in LoginWithJwtFilter is null!");
        }


        if( claims != null ){
            String username = claims.getSubject();

            System.out.println("Username: "+username);

            UserDetails userDetails = chatroomUserDetailsServiceImpl.loadUserByUsername(username);

            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());

            System.out.println("Authorities:"+authentication.getAuthorities());

            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        filterChain.doFilter(request, response);
    }
}
