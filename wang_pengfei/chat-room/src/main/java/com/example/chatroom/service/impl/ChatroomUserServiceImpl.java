package com.example.chatroom.service.impl;

import com.example.chatroom.entity.ChatroomUser;
import com.example.chatroom.repository.ChatroomUserRepository;
import com.example.chatroom.service.ChatroomUserService;
import com.example.chatroom.util.JwtUtil;
import com.example.chatroom.vo.ChatroomUserVO;
import com.example.chatroom.vo.JsonResult;
import com.example.chatroom.vo.LoginAndRegParam;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ChatroomUserServiceImpl implements ChatroomUserService {
    @Resource
    private ChatroomUserRepository chatroomUserRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public List<ChatroomUserVO> findAll() {
        List<ChatroomUserVO> chatroomUserVOList = chatroomUserRepository.findAll().stream().map(chatroomUser -> {
            ChatroomUserVO chatroomUserVO = new ChatroomUserVO();
            chatroomUserVO.setId(chatroomUser.getId());
            chatroomUserVO.setUsername(chatroomUser.getUsername());
            return chatroomUserVO;
        }).collect(Collectors.toList());

        return chatroomUserVOList;
    }

    @Override
    public Optional<ChatroomUser> getByUsername(@NonNull String username) {
        List<ChatroomUser> res = chatroomUserRepository.getByUsername(username);

        return Optional.ofNullable(res.isEmpty() ? null : res.get(0));
    }

    @Override
    public boolean create(@NonNull ChatroomUserVO chatroomUserVO) {
        String username = chatroomUserVO.getUsername();
        String password = chatroomUserVO.getPassword();

        List<ChatroomUser> sameName = chatroomUserRepository.getByUsername(username);
        if( !sameName.isEmpty() ){
            return false;
        }

        String encryptedPwd = passwordEncoder.encode(password);

        ChatroomUser chatroomUser = ChatroomUser.builder()
                                                .username(username)
                                                .password(encryptedPwd)
                                                .build();

        try {
            chatroomUser = chatroomUserRepository.save(chatroomUser);
            chatroomUserVO.setId(chatroomUser.getId());
            return true;
        }catch (Exception ee){
            System.err.println("Error when registering a  user!");
            ee.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(@NonNull Long id) {
        try {
            chatroomUserRepository.deleteById(id);
            return true;
        }catch (Exception ee){
            return false;
        }
    }

    @Override
    public ChatroomUser getById(@NonNull Long id) {
        return chatroomUserRepository.getReferenceById(id);
    }

    @Override
    public JsonResult login(LoginAndRegParam loginAndRegParam) {
        List<ChatroomUser> chatroomUserList = chatroomUserRepository.getByUsername(loginAndRegParam.getUsername());
        if( chatroomUserList.isEmpty() ||
                !passwordEncoder.matches(loginAndRegParam.getPassword(), chatroomUserList.get(0).getPassword()) ){
            return new JsonResult(JsonResult.JsonResultCode.ERROR, "Login failed.", null);
        }

        return new JsonResult(JsonResult.JsonResultCode.SUCCESS,
                chatroomUserList.get(0).getId().toString(), jwtUtil.generate(chatroomUserList.get(0).getUsername()));
    }
}
