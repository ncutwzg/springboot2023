package com.example.chatroom.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Date;

@Component
public class JwtUtil {
    private String secretKey = "chatroomSecretKey_long_long_long_long_long_long_long_long_long_long";

    private Duration expiration = Duration.ofMinutes(30);

    public String generate(String username){
        Date expiryDate = new Date(System.currentTimeMillis() + expiration.toMillis());

        SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8));

        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    public Claims parse(String token){
        System.out.println("Token as param in JwtUtil.parse is:"+ token);

        if( token == null || token.isEmpty() ){
            System.out.println("Token as param in JwtUtil.parse is null or empty!");
            return null;
        }

        Claims claims = null;

        try {
            SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8));

            claims = Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
        }catch (JwtException ee){
            System.err.println("JWT parse error.");
            ee.printStackTrace();
        }

        if(claims == null){
            System.out.println("Get null claims in JwtUtil.parse");
        }else {
            System.out.println("Claims is not null in JwtUtil.parse");
        }

        return claims;
    }
}
